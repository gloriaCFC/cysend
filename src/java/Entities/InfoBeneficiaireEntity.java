/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author g.mbuyi
 */
public class InfoBeneficiaireEntity {
    
    String name;
    String type;
    String description;
    String pattern;
    boolean required;
    String value;

    public InfoBeneficiaireEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
    public Map<String , InfoBeneficiaireEntity> TomapList(List<InfoBeneficiaireEntity> list)
    {
        Map<String , InfoBeneficiaireEntity> map = new TreeMap<>();
        
        if(list!=null)
        {
            for(InfoBeneficiaireEntity info : list)
            {
                map.put(info.name, info);
            }
        }
        
        return map;
    }
    
    
    
    public static InfoBeneficiaireEntity getInstanceFromJson(String json){
        InfoBeneficiaireEntity obj = new Gson().fromJson(json, new TypeToken<InfoBeneficiaireEntity>(){}.getType());
        return obj;
    }
    
}
