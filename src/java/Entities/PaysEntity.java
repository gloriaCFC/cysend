/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class PaysEntity {
    
    int country_id;
    String country_name;
    int country_code;
    String country_iso_alpha2;
    String country_iso_alpha3;
    boolean promotion;
    List<Integer> category_ids;

    public PaysEntity() {
        category_ids = new ArrayList<>();
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public int getCountry_code() {
        return country_code;
    }

    public void setCountry_code(int country_code) {
        this.country_code = country_code;
    }

    public String getCountry_iso_alpha2() {
        return country_iso_alpha2;
    }

    public void setCountry_iso_alpha2(String country_iso_alpha2) {
        this.country_iso_alpha2 = country_iso_alpha2;
    }

    public String getCountry_iso_alpha3() {
        return country_iso_alpha3;
    }

    public void setCountry_iso_alpha3(String country_iso_alpha3) {
        this.country_iso_alpha3 = country_iso_alpha3;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    public List<Integer> getCategory_ids() {
        return category_ids;
    }

    public void setCategory_ids(List<Integer> category_ids) {
        this.category_ids = category_ids;
    }
    
    
    
    public static PaysEntity getInstanceFromJson(String json){
        PaysEntity obj = new Gson().fromJson(json, new TypeToken<PaysEntity>(){}.getType());
        return obj;
    }
    
}
