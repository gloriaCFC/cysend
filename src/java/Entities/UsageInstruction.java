/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class UsageInstruction {
    
    private String language;
    private String text;

    public UsageInstruction() {
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    public static UsageInstruction getInstanceFromJson(String json){
        UsageInstruction obj = new Gson().fromJson(json, new TypeToken<UsageInstruction>(){}.getType());
        return obj;
    }
    
}
