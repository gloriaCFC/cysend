/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author g.mbuyi
 */
public class ProduitCyEntity {
    
    private Long idProduit;
    private String nom;
    private String logo;
    private String maintenance;
    private String typeproduit;

    public ProduitCyEntity() {
    }

    public ProduitCyEntity(Long idProduit, String nom, String logo, String maintenance, String typeproduit) {
        this.idProduit = idProduit;
        this.nom = nom;
        this.logo = logo;
        this.maintenance = maintenance;
        this.typeproduit = typeproduit;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(String maintenance) {
        this.maintenance = maintenance;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }
    
    
    
    
}
