/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import RequestResponseEntities.GetPaysRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class TransactionEntity {
    
    
    private Long id;
    private String date;
    private String codeAgence;
    private String nomAgence;
    private String agent;
    private String posid;
    private Double montant;
    private Double frais;
    private Double montantTotal;
    private String devise;
    private Double commission;
    private Double commissionFcn;
    private String issueRef;
    private String idExterne;
    private String statut;
    private String beneficiaire;

    public TransactionEntity(Long id, String date, String codeAgence, String nomAgence, String agent, String posid, Double montant, Double frais, Double montantTotal, String devise, Double commission, Double commissionFcn, String issueRef, String idExterne, String statut) {
        this.id = id;
        this.date = date;
        this.codeAgence = codeAgence;
        this.nomAgence = nomAgence;
        this.agent = agent;
        this.posid = posid;
        this.montant = montant;
        this.frais = frais;
        this.montantTotal = montantTotal;
        this.devise = devise;
        this.commission = commission;
        this.commissionFcn = commissionFcn;
        this.issueRef = issueRef;
        this.idExterne = idExterne;
        this.statut = statut;
    }
    
    

    public TransactionEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCodeAgence() {
        return codeAgence;
    }

    public void setCodeAgence(String codeAgence) {
        this.codeAgence = codeAgence;
    }

    public String getNomAgence() {
        return nomAgence;
    }

    public void setNomAgence(String nomAgence) {
        this.nomAgence = nomAgence;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPosid() {
        return posid;
    }

    public void setPosid(String posid) {
        this.posid = posid;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    public Double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(Double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getCommissionFcn() {
        return commissionFcn;
    }

    public void setCommissionFcn(Double commissionFcn) {
        this.commissionFcn = commissionFcn;
    }

    public String getIssueRef() {
        return issueRef;
    }

    public void setIssueRef(String issueRef) {
        this.issueRef = issueRef;
    }

    public String getIdExterne() {
        return idExterne;
    }

    public void setIdExterne(String idExterne) {
        this.idExterne = idExterne;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(String beneficiaire) {
        this.beneficiaire = beneficiaire;
    }
    
    
    
    public static TransactionEntity getInstanceFromJson(String json){
        TransactionEntity obj = new Gson().fromJson(json, new TypeToken<TransactionEntity>(){}.getType());
        return obj;
    }
    
    
}
