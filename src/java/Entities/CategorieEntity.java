/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class CategorieEntity {

    Long category_id;
    String category_name;
    boolean promotion;
    List<Long> product_ids;
    
    public CategorieEntity() {
        product_ids = new ArrayList<>();
    }

    
    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public List<Long> getProduct_ids() {
        return product_ids;
    }

    public void setProduct_ids(List<Long> product_ids) {
        this.product_ids = product_ids;
    }

    
    
    public static CategorieEntity getInstanceFromJson(String json){
        CategorieEntity obj = new Gson().fromJson(json, new TypeToken<CategorieEntity>(){}.getType());
        return obj;
    }
    
    

    
}
