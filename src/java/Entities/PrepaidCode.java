/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class PrepaidCode {
    private String code;
    private String serial;
    private String expiration;
    private List<UsageInstruction> usage_instructions;

    public PrepaidCode() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public List<UsageInstruction> getUsage_instructions() {
        return usage_instructions;
    }

    public void setUsage_instructions(List<UsageInstruction> usage_instructions) {
        this.usage_instructions = usage_instructions;
    }
    
    
    public static PrepaidCode getInstanceFromJson(String json){
        PrepaidCode obj = new Gson().fromJson(json, new TypeToken<PrepaidCode>(){}.getType());
        return obj;
    }
            
            
            
    
}
