/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class CategorieCyEntity {
    
    private Long idCategorie;
    private String nom;
    private List<ProduitCyEntity> listProduit;

    public CategorieCyEntity() {
        listProduit = new ArrayList<>();
    }

    public Long getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Long idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<ProduitCyEntity> getListProduit() {
        return listProduit;
    }

    public void setListProduit(List<ProduitCyEntity> listProduit) {
        this.listProduit = listProduit;
    }
    
    
    
}
