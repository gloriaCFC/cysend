/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author g.mbuyi
 */
public class PaysRequestEntity {
    
    private Long id ;
    private String nom;
    private Long idPaysCy;
    private String codeiso3;
    private String codeiso2;
    private String prefix;

    public PaysRequestEntity() {
    }

    public PaysRequestEntity(Long id, String nom, Long idPaysCy) {
        this.id = id;
        this.nom = nom;
        this.idPaysCy = idPaysCy;
    }

    public PaysRequestEntity(Long id, String nom, Long idPaysCy, String codeiso3) {
        this.id = id;
        this.nom = nom;
        this.idPaysCy = idPaysCy;
        this.codeiso3 = codeiso3;
    }

    public PaysRequestEntity(Long id, String nom, Long idPaysCy, String codeiso3, String codeiso2) {
        this.id = id;
        this.nom = nom;
        this.idPaysCy = idPaysCy;
        this.codeiso3 = codeiso3;
        this.codeiso2 = codeiso2;
    }

    public PaysRequestEntity(Long id, String nom, Long idPaysCy, String codeiso3, String codeiso2, String prefix) {
        this.id = id;
        this.nom = nom;
        this.idPaysCy = idPaysCy;
        this.codeiso3 = codeiso3;
        this.codeiso2 = codeiso2;
        this.prefix = prefix;
    }
    
    
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Long getIdPaysCy() {
        return idPaysCy;
    }

    public void setIdPaysCy(Long idPaysCy) {
        this.idPaysCy = idPaysCy;
    }

    public String getCodeiso3() {
        return codeiso3;
    }

    public void setCodeiso3(String codeiso3) {
        this.codeiso3 = codeiso3;
    }

    public String getCodeiso2() {
        return codeiso2;
    }

    public void setCodeiso2(String codeiso2) {
        this.codeiso2 = codeiso2;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    
    
    
}
