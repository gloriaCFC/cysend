/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class ProduitEntity {
    
    Long product_id;
    String product_name;
    String logo_url;
    String type;
    boolean promotion;
    boolean maintenance;
    List<Long>categories;
    List<Long>countries;
    List<Long> face_value_ids;
    List<InfoBeneficiaireEntity> beneficiary_information;

    public ProduitEntity() {
        
        categories = new ArrayList<>();
        countries = new ArrayList<>();
        beneficiary_information = new ArrayList<>();
        face_value_ids = new ArrayList<>();
    }

    

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }

    

    public List<InfoBeneficiaireEntity> getBeneficiary_information() {
        return beneficiary_information;
    }

    public void setBeneficiary_information(List<InfoBeneficiaireEntity> beneficiary_information) {
        this.beneficiary_information = beneficiary_information;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public List<Long> getCountries() {
        return countries;
    }

    public void setCountries(List<Long> countries) {
        this.countries = countries;
    }

    public List<Long> getFace_value_ids() {
        return face_value_ids;
    }

    public void setFace_value_ids(List<Long> face_value_ids) {
        this.face_value_ids = face_value_ids;
    }

    

    public static ProduitEntity getInstanceFromJson(String json){
        ProduitEntity obj = new Gson().fromJson(json, new TypeToken<ProduitEntity>(){}.getType());
        return obj;
    }
    
            
    
}
