/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import cfcdatabase.Model.Facevaluecy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class PrixEntity {
    
     private Long id;
     private Date date;
     private Long facevalueid;
     private Double facevalue;
     private Double cost;
     private String promotion;
     private String typefacevalue;
     private Double facevaluefrom;
     private Double facevalueto;
     private Double facevaluestep;
     private String facevaluecurrency;
     private Double minimumcost;
     private Double maximumcost;
     private String costcurrency;
     private String statut;

    public PrixEntity() {
    }

    public PrixEntity(Long id, Date date, Long facevalueid, Double facevalue, Double cost, String promotion, String typefacevalue, Double facevaluefrom, Double facevalueto, Double facevaluestep, String facevaluecurrency, Double minimumcost, Double maximumcost, String costcurrency, String statut) {
        this.id = id;
        this.date = date;
        this.facevalueid = facevalueid;
        this.facevalue = facevalue;
        this.cost = cost;
        this.promotion = promotion;
        this.typefacevalue = typefacevalue;
        this.facevaluefrom = facevaluefrom;
        this.facevalueto = facevalueto;
        this.facevaluestep = facevaluestep;
        this.facevaluecurrency = facevaluecurrency;
        this.minimumcost = minimumcost;
        this.maximumcost = maximumcost;
        this.costcurrency = costcurrency;
        this.statut = statut;
    }
    
    public PrixEntity(Facevaluecy face)
    {
        this.id = face.getId();
        this.date = face.getDate();
        this.facevalueid = face.getFacevalueid();
        this.facevalue = face.getFacevalue();
        this.cost = face.getCost();
        this.promotion = face.getPromotion();
        this.typefacevalue = face.getTypefacevalue();
        this.facevaluefrom = face.getFacevaluefrom();
        this.facevalueto = face.getFacevalueto();
        this.facevaluestep = face.getFacevaluestep();
        this.facevaluecurrency = face.getFacevaluecurrency();
        this.minimumcost = face.getMinimumcost();
        this.maximumcost = face.getMaximumcost();
        this.costcurrency = face.getCostcurrency();
        this.statut = face.getStatut();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getFacevalueid() {
        return facevalueid;
    }

    public void setFacevalueid(Long facevalueid) {
        this.facevalueid = facevalueid;
    }

    public Double getFacevalue() {
        return facevalue;
    }

    public void setFacevalue(Double facevalue) {
        this.facevalue = facevalue;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getTypefacevalue() {
        return typefacevalue;
    }

    public void setTypefacevalue(String typefacevalue) {
        this.typefacevalue = typefacevalue;
    }

    public Double getFacevaluefrom() {
        return facevaluefrom;
    }

    public void setFacevaluefrom(Double facevaluefrom) {
        this.facevaluefrom = facevaluefrom;
    }

    public Double getFacevalueto() {
        return facevalueto;
    }

    public void setFacevalueto(Double facevalueto) {
        this.facevalueto = facevalueto;
    }

   

    public Double getFacevaluestep() {
        return facevaluestep;
    }

    public void setFacevaluestep(Double facevaluestep) {
        this.facevaluestep = facevaluestep;
    }

    public String getFacevaluecurrency() {
        return facevaluecurrency;
    }

    public void setFacevaluecurrency(String facevaluecurrency) {
        this.facevaluecurrency = facevaluecurrency;
    }

    public Double getMinimumcost() {
        return minimumcost;
    }

    public void setMinimumcost(Double minimumcost) {
        this.minimumcost = minimumcost;
    }

    public Double getMaximumcost() {
        return maximumcost;
    }

    public void setMaximumcost(Double maximumcost) {
        this.maximumcost = maximumcost;
    }

    public String getCostcurrency() {
        return costcurrency;
    }

    public void setCostcurrency(String costcurrency) {
        this.costcurrency = costcurrency;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
     
     public List<PrixEntity> getList(List<Facevaluecy> f)
     {
         List<PrixEntity> list = new ArrayList<>();
         for(Facevaluecy face : f)
         {
             list.add(new PrixEntity(face));
         }
         
         return list;
     }
    
    
}
