/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class FaceValueEntity {
    
    int face_value_id;
    int product_id;
    Double face_value;
    Double face_value_from;
    Double face_value_to;
    Double face_value_step;
    Double minimum_cost;
    Double maximum_cost;
    String cost_currency;
    String face_value_currency;
    Double cost;
    String definition;
    boolean promotion;

    public FaceValueEntity() {
    }

    public int getFace_value_id() {
        return face_value_id;
    }

    public void setFace_value_id(int face_value_id) {
        this.face_value_id = face_value_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Double getFace_value() {
        return face_value;
    }

    public void setFace_value(Double face_value) {
        this.face_value = face_value;
    }

    public Double getFace_value_from() {
        return face_value_from;
    }

    public void setFace_value_from(Double face_value_from) {
        this.face_value_from = face_value_from;
    }

    public Double getFace_value_to() {
        return face_value_to;
    }

    public void setFace_value_to(Double face_value_to) {
        this.face_value_to = face_value_to;
    }
    
    
    
    public Double getFace_value_step() {
        return face_value_step;
    }

    public void setFace_value_step(Double face_value_step) {
        this.face_value_step = face_value_step;
    }

    public Double getMinimum_cost() {
        return minimum_cost;
    }

    public void setMinimum_cost(Double minimum_cost) {
        this.minimum_cost = minimum_cost;
    }

    public Double getMaximum_cost() {
        return maximum_cost;
    }

    public void setMaximum_cost(Double maximum_cost) {
        this.maximum_cost = maximum_cost;
    }

    public String getCost_currency() {
        return cost_currency;
    }

    public void setCost_currency(String cost_currency) {
        this.cost_currency = cost_currency;
    }

    public String getFace_value_currency() {
        return face_value_currency;
    }

    public void setFace_value_currency(String face_value_currency) {
        this.face_value_currency = face_value_currency;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }
    
    public static FaceValueEntity getInstanceFromJson(String json){
        FaceValueEntity obj = new Gson().fromJson(json, new TypeToken<FaceValueEntity>(){}.getType());
        return obj;
    }
    
}
