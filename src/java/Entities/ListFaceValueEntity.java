/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author g.mbuyi
 */
public class ListFaceValueEntity {
    
    Map<Long , FaceValueEntity> listRange ;
    Map<Long,FaceValueEntity> listFixed ;

    public ListFaceValueEntity() {
        listFixed = new TreeMap<>();
        listRange = new TreeMap<>();;
    }

    public Map<Long, FaceValueEntity> getListRange() {
        return listRange;
    }

    public void setListRange(Map<Long, FaceValueEntity> listRange) {
        this.listRange = listRange;
    }

    public Map<Long, FaceValueEntity> getListFixed() {
        return listFixed;
    }

    public void setListFixed(Map<Long, FaceValueEntity> listFixed) {
        this.listFixed = listFixed;
    }

    
    
    
    
    
}
