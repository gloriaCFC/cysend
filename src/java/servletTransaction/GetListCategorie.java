/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servletTransaction;

import Controlleur.ControlleurRequest;
import RequestResponseEntities.ListProduitCategorieRequest;
import RequestResponseEntities.ListProduitCategorieResponse;
import cfcdatabase.Model.Agent;
import daomanagers.ControleurHabilitationconfig;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.SessionUtils;
import utility.ErrorResponse;
import utility.HttpDataResponse;
import utility.HttpUtility;

/**
 *
 * @author g.mbuyi
 */
@WebServlet( urlPatterns = {"/GetListCategorie"})
public class GetListCategorie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String json = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        
        boolean isOk = true;
        
        System.out.println("--------JSON Request : "+json);
        ListProduitCategorieRequest re = ListProduitCategorieRequest.getInstanceFromJson(json);
        
        Agent agent = null;
        HttpDataResponse<ListProduitCategorieResponse> responseEntity = new HttpDataResponse<>();
        ErrorResponse errorResponse = new ErrorResponse("500","ECHEC!");
        //Block de verification des données envoyées ---------------------------
        if(re != null){
            SessionUtils su = new SessionUtils();
            entities.AuthentificationResultEntity authentificationResultEntity = su.authentifyV2(re.getMobile() != null, request, null , re.getToken(), true);
            if(authentificationResultEntity.getAgent() == null){
                isOk = false;
                errorResponse.setCode(authentificationResultEntity.getCode());
                errorResponse.setDescription(authentificationResultEntity.getMsg());
            }
            else
                agent = authentificationResultEntity.getAgent();
        }
        else{
            isOk = false;
            errorResponse.setCode("400");
            errorResponse.setDescription("PARAMETRES MANQUANT");
        }
        
        ControleurHabilitationconfig chf = new ControleurHabilitationconfig();
        Boolean produitOn = chf.produitDisponible(ControleurHabilitationconfig.NOM_CYSEND);

        if(produitOn == false)
        {
            isOk = false;
            errorResponse.setCode("466");
            errorResponse.setDescription("ECHEC DE LA TRANSACTION : LES OPERATIONS AVEC CYSEND  SONT INDISPONIBLES");
            
        }
        
        if(isOk){
            
            try {
                ControlleurRequest cb = new ControlleurRequest();
                responseEntity.setResponse(cb.GetCategorie(re.getCodeiso3()));
                responseEntity.setError(new ErrorResponse("200", "OK"));
            } catch (Exception e) {
                errorResponse.setCode("500");
                errorResponse.setDescription(e.toString());
                responseEntity.setError(errorResponse);
            
                e.printStackTrace();
            }
            
            
        }
        else
        {
            responseEntity.setError(errorResponse);
        }
        
        ServletOutputStream out = HttpUtility.prepareHttpResponseV1(response, responseEntity);
        out.flush();
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
