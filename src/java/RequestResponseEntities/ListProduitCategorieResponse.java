/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.CategorieCyEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author g.mbuyi
 */
public class ListProduitCategorieResponse {
    
    private List<CategorieCyEntity> listCat;

    public ListProduitCategorieResponse() {
        
        listCat = new ArrayList<>();
    }

    public List<CategorieCyEntity> getListCat() {
        return listCat;
    }

    public void setListCat(List<CategorieCyEntity> listCat) {
        this.listCat = listCat;
    }
    
    
}
