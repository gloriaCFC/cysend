/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class GetFaceValueRequest {
    
    private String mobile;
    private String token;
    private Long idProduit;

    public GetFaceValueRequest() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }
    
    public static GetFaceValueRequest getInstanceFromJson(String json){
        GetFaceValueRequest obj = new Gson().fromJson(json, new TypeToken<GetFaceValueRequest>(){}.getType());
        return obj;
    }
    
    
}
