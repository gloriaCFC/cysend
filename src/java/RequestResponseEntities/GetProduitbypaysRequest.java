/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class GetProduitbypaysRequest {
    
    private String mobile;
    private String token;
    private String codeiso;
    private String nom;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCodeiso() {
        return codeiso;
    }

    public void setCodeiso(String codeiso) {
        this.codeiso = codeiso;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    
    
    public static GetProduitbypaysRequest getInstanceFromJson(String json){
        GetProduitbypaysRequest obj = new Gson().fromJson(json, new TypeToken<GetProduitbypaysRequest>(){}.getType());
        return obj;
    }
    
    
}
