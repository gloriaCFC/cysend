/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class ParamettreRequest {
    
    private String token;
    private Long idProduit;
    private Double frais;

    public ParamettreRequest() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }
    
    public static ParamettreRequest getInstanceFromJson(String json){
        ParamettreRequest obj = new Gson().fromJson(json, new TypeToken<ParamettreRequest>(){}.getType());
        return obj;
    }
    
    
}
