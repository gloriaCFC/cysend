/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.InfoBeneficiaireEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class SendOrderTelcoRequest {
    
    private String mobile;
    private String token;
    private Long idFacevalue;
    private List<InfoBeneficiaireEntity> listInfoBene;
    private String deviseTransaction;
    private Double step;
    private Long idAgentRemote;
    private Long idPays;

    public SendOrderTelcoRequest() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdFacevalue() {
        return idFacevalue;
    }

    public void setIdFacevalue(Long idFacevalue) {
        this.idFacevalue = idFacevalue;
    }

    public List<InfoBeneficiaireEntity> getListInfoBene() {
        return listInfoBene;
    }

    public void setListInfoBene(List<InfoBeneficiaireEntity> listInfoBene) {
        this.listInfoBene = listInfoBene;
    }

    public String getDeviseTransaction() {
        return deviseTransaction;
    }

    public void setDeviseTransaction(String deviseTransaction) {
        this.deviseTransaction = deviseTransaction;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }

    public Long getIdAgentRemote() {
        return idAgentRemote;
    }

    public void setIdAgentRemote(Long idAgentRemote) {
        this.idAgentRemote = idAgentRemote;
    }

    public Long getIdPays() {
        return idPays;
    }

    public void setIdPays(Long idPays) {
        this.idPays = idPays;
    }
    
    public static SendOrderTelcoRequest getInstanceFromJson(String json){
        SendOrderTelcoRequest obj = new Gson().fromJson(json, new TypeToken<SendOrderTelcoRequest>(){}.getType());
        return obj;
    }
    
    
}
