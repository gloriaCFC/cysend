/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class GetListProduitRequest {
    
    private String mobile;
    private String token;
    private Long idCategorie;
    private String codeiso3;

    public GetListProduitRequest() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Long idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getCodeiso3() {
        return codeiso3;
    }

    public void setCodeiso3(String codeiso3) {
        this.codeiso3 = codeiso3;
    }

    
    
    public static GetListProduitRequest getInstanceFromJson(String json){
        GetListProduitRequest obj = new Gson().fromJson(json, new TypeToken<GetListProduitRequest>(){}.getType());
        return obj;
    }
    
}
