/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class ListProduitCategorieRequest {
    
    private String codeiso3;
    private String mobile;
    private String token;

    public ListProduitCategorieRequest() {
    }
    
    

    public String getCodeiso3() {
        return codeiso3;
    }

    public void setCodeiso3(String codeiso3) {
        this.codeiso3 = codeiso3;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    
    public static ListProduitCategorieRequest getInstanceFromJson(String json){
        ListProduitCategorieRequest obj = new Gson().fromJson(json, new TypeToken<ListProduitCategorieRequest>(){}.getType());
        return obj;
    }
    
    
}
