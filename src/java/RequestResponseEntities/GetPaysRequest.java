/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class GetPaysRequest {
    
    private String mobile;
    private String token;

    public GetPaysRequest() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    
    
    public static GetPaysRequest getInstanceFromJson(String json){
        GetPaysRequest obj = new Gson().fromJson(json, new TypeToken<GetPaysRequest>(){}.getType());
        return obj;
    }
    
    
}
