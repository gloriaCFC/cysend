/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.InfoBeneficiaireEntity;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class EstimationResponse {
    
    private Long idFacevalue;
    private Double montant;
    private Double montantTotal;
    private Double fraisSms;
    private String deviseTransaction;
    private List<InfoBeneficiaireEntity> listInfoBene;
    private Double frais ;
    private Double facevalueChoisi;

    public EstimationResponse() {
    }

    public EstimationResponse(Long idFacevalue, Double montant, String deviseTransaction) {
        this.idFacevalue = idFacevalue;
        this.montant = montant;
        this.deviseTransaction = deviseTransaction;
    }

    public Long getIdFacevalue() {
        return idFacevalue;
    }

    public void setIdFacevalue(Long idFacevalue) {
        this.idFacevalue = idFacevalue;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getDeviseTransaction() {
        return deviseTransaction;
    }

    public void setDeviseTransaction(String deviseTransaction) {
        this.deviseTransaction = deviseTransaction;
    }

    public List<InfoBeneficiaireEntity> getListInfoBene() {
        return listInfoBene;
    }

    public void setListInfoBene(List<InfoBeneficiaireEntity> listInfoBene) {
        this.listInfoBene = listInfoBene;
    }

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    public Double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(Double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public Double getFacevalueChoisi() {
        return facevalueChoisi;
    }

    public void setFacevalueChoisi(Double facevalueChoisi) {
        this.facevalueChoisi = facevalueChoisi;
    }

    public Double getFraisSms() {
        return fraisSms;
    }

    public void setFraisSms(Double fraisSms) {
        this.fraisSms = fraisSms;
    }

    @Override
    public String toString() {
        return "EstimationResponse{" + "idFacevalue=" + idFacevalue + ", montant=" + montant + ", montantTotal=" + montantTotal + ", fraisSms=" + fraisSms + ", deviseTransaction=" + deviseTransaction + ", listInfoBene=" + listInfoBene + ", frais=" + frais + ", facevalueChoisi=" + facevalueChoisi + '}';
    }

    
    
    
    

   

    
    
    
    
}
