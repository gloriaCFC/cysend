/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.PaysRequestEntity;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class GetPaysResponse {
    
    private List<PaysRequestEntity> listPays;

    public GetPaysResponse() {
        listPays = new ArrayList<>();
    }

    public GetPaysResponse(List<PaysRequestEntity> listPays) {
        this.listPays = listPays;
    }
    
    

    public List<PaysRequestEntity> getListPays() {
        return listPays;
    }

    public void setListPays(List<PaysRequestEntity> listPays) {
        this.listPays = listPays;
    }
    
    
    
}
