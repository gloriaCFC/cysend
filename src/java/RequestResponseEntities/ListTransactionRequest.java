/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class ListTransactionRequest {
    
    private String login;
    private Long idagent;
    private String codeagence;
    private String date0;
    private String date1;
    private Long idville;
    private Long idregion;
    private Long idpays;
    private String mobile;
    private String posid;
    private Long   iddevise;
    private String deviseiso;
    private String statut;
    private String token;

    public ListTransactionRequest() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getIdagent() {
        return idagent;
    }

    public void setIdagent(Long idagent) {
        this.idagent = idagent;
    }

    public String getCodeagence() {
        return codeagence;
    }

    public void setCodeagence(String codeagence) {
        this.codeagence = codeagence;
    }

    public String getDate0() {
        return date0;
    }

    public void setDate0(String date0) {
        this.date0 = date0;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public Long getIdville() {
        return idville;
    }

    public void setIdville(Long idville) {
        this.idville = idville;
    }

    public Long getIdregion() {
        return idregion;
    }

    public void setIdregion(Long idregion) {
        this.idregion = idregion;
    }

    public Long getIdpays() {
        return idpays;
    }

    public void setIdpays(Long idpays) {
        this.idpays = idpays;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPosid() {
        return posid;
    }

    public void setPosid(String posid) {
        this.posid = posid;
    }

    public Long getIddevise() {
        return iddevise;
    }

    public void setIddevise(Long iddevise) {
        this.iddevise = iddevise;
    }

    public String getDeviseiso() {
        return deviseiso;
    }

    public void setDeviseiso(String deviseiso) {
        this.deviseiso = deviseiso;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    
    
    
    public static ListTransactionRequest getInstanceFromJson(String json){
        ListTransactionRequest obj = new Gson().fromJson(json, new TypeToken<ListTransactionRequest>(){}.getType());
        return obj;
    }
}
