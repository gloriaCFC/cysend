/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class DeviseRequestEntity {
    
    private String token;
    private String mobile;
    private Long idAgent;

    public DeviseRequestEntity() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(Long idAgent) {
        this.idAgent = idAgent;
    }

    
    
    public static DeviseRequestEntity getInstanceFromJson(String json){
        DeviseRequestEntity obj = new Gson().fromJson(json, new TypeToken<DeviseRequestEntity>(){}.getType());
        return obj;
    }
    
}
