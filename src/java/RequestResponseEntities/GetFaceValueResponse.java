/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.PrixEntity;
import cfcdatabase.Model.Facevaluecy;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class GetFaceValueResponse {
    
    private List<PrixEntity> prix;

    public GetFaceValueResponse() {
    }

    public GetFaceValueResponse(List<PrixEntity> prix) {
        this.prix = prix;
    }

    public List<PrixEntity> getPrix() {
        return prix;
    }

    public void setPrix(List<PrixEntity> prix) {
        this.prix = prix;
    }

    
    
    
    
}
