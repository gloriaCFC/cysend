/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class ForceCallbackRequest {
    
    private String mobile;
    private String token;
    private Long idTransaction;

    public ForceCallbackRequest() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(Long idTransaction) {
        this.idTransaction = idTransaction;
    }
    
    public static ForceCallbackRequest getInstanceFromJson(String json){
        ForceCallbackRequest obj = new Gson().fromJson(json, new TypeToken<ForceCallbackRequest>(){}.getType());
        return obj;
    }
    
    
    
}
