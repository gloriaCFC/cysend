/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.InfoBeneficiaireEntity;
import Entities.PrepaidCode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class SendOrderResponse {
    
    private String uid;
    private String user_uid;
    private String date;
    private int face_value_id;
    private Double face_value;
    private String face_value_currency;
    private int gift_card_code;
    private String scenario;
    private String callback_url;
    private Double cost;
    private String currency;
    private List<InfoBeneficiaireEntity> beneficiary_information;
    private String issuer_reference;
    private PrepaidCode prepaid_code;
    private String status;
    private String response_code;
    private String response_title;
    private String response_description;

    public SendOrderResponse() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUser_uid() {
        return user_uid;
    }

    public void setUser_uid(String user_uid) {
        this.user_uid = user_uid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getFace_value_id() {
        return face_value_id;
    }

    public void setFace_value_id(int face_value_id) {
        this.face_value_id = face_value_id;
    }

    public Double getFace_value() {
        return face_value;
    }

    public void setFace_value(Double face_value) {
        this.face_value = face_value;
    }

    public String getFace_value_currency() {
        return face_value_currency;
    }

    public void setFace_value_currency(String face_value_currency) {
        this.face_value_currency = face_value_currency;
    }

    public int getGift_card_code() {
        return gift_card_code;
    }

    public void setGift_card_code(int gift_card_code) {
        this.gift_card_code = gift_card_code;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<InfoBeneficiaireEntity> getBeneficiary_information() {
        return beneficiary_information;
    }

    public void setBeneficiary_information(List<InfoBeneficiaireEntity> beneficiary_information) {
        this.beneficiary_information = beneficiary_information;
    }

    public String getIssuer_reference() {
        return issuer_reference;
    }

    public void setIssuer_reference(String issuer_reference) {
        this.issuer_reference = issuer_reference;
    }

    public PrepaidCode getPrepaid_code() {
        return prepaid_code;
    }

    public void setPrepaid_code(PrepaidCode prepaid_code) {
        this.prepaid_code = prepaid_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getResponse_title() {
        return response_title;
    }

    public void setResponse_title(String response_title) {
        this.response_title = response_title;
    }

    public String getResponse_description() {
        return response_description;
    }

    public void setResponse_description(String response_description) {
        this.response_description = response_description;
    }
    
    
    public static SendOrderResponse getInstanceFromJson(String json){
        SendOrderResponse obj = new Gson().fromJson(json, new TypeToken<SendOrderResponse>(){}.getType());
        return obj;
    }
    
    
}
