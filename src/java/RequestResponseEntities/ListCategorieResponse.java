/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import Entities.CategorieCyEntity;
import java.util.List;

/**
 *
 * @author g.mbuyi
 */
public class ListCategorieResponse {
    
    private List<CategorieCyEntity> listCat;

    public ListCategorieResponse(List<CategorieCyEntity> listCat) {
        this.listCat = listCat;
    }

    public List<CategorieCyEntity> getListCat() {
        return listCat;
    }

    public void setListCat(List<CategorieCyEntity> listCat) {
        this.listCat = listCat;
    }
    
    
    
}
