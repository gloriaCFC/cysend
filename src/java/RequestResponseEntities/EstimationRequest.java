/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestResponseEntities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class EstimationRequest {
    
    private String mobile;
    private String token;
    private Long idFacevalue;
    private Double step;
    private String deviseTransaction;
    private String notification;

    public EstimationRequest() {
    }
    
    

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getIdFacevalue() {
        return idFacevalue;
    }

    public void setIdFacevalue(Long idFacevalue) {
        this.idFacevalue = idFacevalue;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }

    public String getDeviseTransaction() {
        return deviseTransaction;
    }

    public void setDeviseTransaction(String deviseTransaction) {
        this.deviseTransaction = deviseTransaction;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
    
    
    
    public static EstimationRequest getInstanceFromJson(String json){
        EstimationRequest obj = new Gson().fromJson(json, new TypeToken<EstimationRequest>(){}.getType());
        return obj;
    }
    
}
