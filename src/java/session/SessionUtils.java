package session;

import cfcdatabase.DAO.DaoConnexionTrack;
import cfcdatabase.Model.Agent;
import cfcdatabase.Model.Connexiontrack;
import controller.Authentification;
import daomanagers.ControlleurAgent;
import daomanagers.ControlleurConnexionTrack;
import entities.AuthentificationResultEntity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import utility.Utils;

/**
 *
 * @author c.kabena
 */
public class SessionUtils {
    
    public static final String KEY_LOGIN        = "username";
    public static final String KEY_IDAGENT      = "idAgent";
    public static final String KEY_ROLE         = "role";
    public static final String KEY_CODEAGENCE   = "codeAgence";
    public static final String KEY_AGENT        = "agentkey";
    
    public AuthentificationResultEntity authentify(boolean isFromMobile, HttpServletRequest request){
        return authentifyV1(isFromMobile, request, null, null);
    }
    public AuthentificationResultEntity authentifyV1(boolean isFromMobile, HttpServletRequest request, Long idagent, String commontoken){
        return authentifyV2(isFromMobile, request, idagent, commontoken, false);
    }
    public AuthentificationResultEntity authentifyV2(boolean isFromMobile, HttpServletRequest request, Long idagent, String commontoken, boolean isCheckOnlyWithToken){
        AuthentificationResultEntity authentificationResultEntity = new AuthentificationResultEntity("500", "echec", null);   
        
        if(!isFromMobile && isCheckOnlyWithToken){
            if(commontoken == null)
                authentificationResultEntity.setCodeAndMsg("0","SESSION EXPIREE 0");
            else{
                
                System.out.println("---------Token : "+commontoken);
                String sql="FROM Connexiontrack obj LEFT JOIN FETCH obj.agent agt LEFT JOIN FETCH agt.agence agc "
                + " WHERE obj.token = :token"
                + " ORDER BY obj.date DESC";
        
                Map<String, Object> params = new HashMap<String, Object>();
        
                params.put("token", commontoken);
        
                List<Connexiontrack> ls = (List<Connexiontrack>) new DaoConnexionTrack().select(sql, params, true, 1);
        
                System.out.println("taille list connexiontrack : "+ls.size());
                if(ls.isEmpty())
                    authentificationResultEntity.setCodeAndMsg("0","SESSION EXPIREE 1");
                else{
                    
                    Connexiontrack connexiontrack = ls.get(0);
                    
                    if(connexiontrack == null || !connexiontrack.getToken().equals(commontoken)){
                        authentificationResultEntity.setCodeAndMsg("0","SESSION EXPIREE 2");
                    }
                    else{
                        authentificationResultEntity.setCodeAndMsg("200", "OK");
                        authentificationResultEntity.setAgent(connexiontrack.getAgent());
                    }
                }
            }
        }
        else if(!isFromMobile && idagent != null && commontoken != null){//WEB
         
            Connexiontrack connexiontrack = new ControlleurConnexionTrack().getLastConnexion(idagent, commontoken);
            
            if(connexiontrack == null || !connexiontrack.getToken().equals(commontoken)){
                authentificationResultEntity.setCodeAndMsg("0","SESSION EXPIREE 3");
            }
            else{
                return initSuccessfulAuth(authentificationResultEntity, idagent);
            }
            
        }
        else if(!isFromMobile){//WEB
                String idAgentStr = ( String ) request.getSession().getAttribute(SessionUtils.KEY_IDAGENT);
                if(idAgentStr == null){
                    authentificationResultEntity.setCodeAndMsg("0","SESSION EXPIREE 4");
                }
                else{
                    return initSuccessfulAuth(authentificationResultEntity, Utils.toLong(idAgentStr));
                }
            }
            else{//MOBILE
                Authentification authentification = new Authentification();
            
                String login = Utils.getDecodedBase64String(request.getHeader("username")) ;
                String pwd = Utils.getDecodedBase64String(request.getHeader("pwd")) ;
                String pin = Utils.getDecodedBase64String(request.getHeader("pin")) ;
                String token = Utils.getDecodedBase64String(request.getHeader("token")) ;
            
                String authorisation = login+":"+pwd+":"+pin+":"+token;
                
                Map<String,Object> map = authentification.getAuthentifiedAgentFromString(authorisation,true,true);
            
                authentificationResultEntity.setCodeAndMsg((String)map.get("resultCode"), (String)map.get("msg"));
                authentificationResultEntity.setAgent((Agent)map.get("agent"));
            }
            
            return authentificationResultEntity;
    }
    
    private AuthentificationResultEntity initSuccessfulAuth(AuthentificationResultEntity authentificationResultEntity, Long idagent){
        Agent agent = new ControlleurAgent().getAgentWithRoles(idagent);
        if(agent == null) {  authentificationResultEntity.setCodeAndMsg("404","UTILISATEUR INTROUVABLE");   }
        else{
            authentificationResultEntity.setCodeAndMsg("200","OK");
            authentificationResultEntity.setAgent(agent);
        } 
                    
        return authentificationResultEntity;
    }
}
