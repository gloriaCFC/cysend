package session;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 *
 * @author c.kabena
 */
public class SessionBinder implements HttpSessionBindingListener{
    
    public static final String SESSION_KEY = "UserContext";
    
    
    
    private String accountId;
    private boolean alreadyLoggedIn;
    private boolean hadPreviousSession;
    private static Map<SessionBinder, HttpSession> logins = new HashMap<SessionBinder, HttpSession>();
    
    public SessionBinder(String accountId){
        this.accountId = accountId;
    }
    
    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        //Block next 
        /*
         HttpSession oldSession = logins.get(this);
        if (oldSession != null) {
            alreadyLoggedIn = true;
        } else {
            logins.put(this, event.getSession());
        }
        */
        
        //Invalidate previous
        HttpSession session = logins.remove(this);
        if (session != null) {
            hadPreviousSession = true;
            session.invalidate();
        }
        logins.put(this, event.getSession()); 
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {}

    @Override
    public boolean equals(Object other) {
        return (other instanceof SessionBinder) && (getAccountId() != null) ? 
        getAccountId().equals(((SessionBinder) other).getAccountId()) : (other == this);
    }
    
    @Override
    public int hashCode() {
        return (getAccountId() != null) ? 
        (this.getClass().hashCode() + getAccountId().hashCode()) : super.hashCode();
    }
    
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isAlreadyLoggedIn() {
        return alreadyLoggedIn;
    }

    public void setAlreadyLoggedIn(boolean isAlreadyLoggedIn) {
        this.alreadyLoggedIn = isAlreadyLoggedIn;
    }
    
    public void invalidateAndRemoveSession(){
        HttpSession session = logins.remove(this);
        if(session != null) session.invalidate();
    }

    public boolean isHadPreviousSession() {
        return hadPreviousSession;
    }

    public void setHadPreviousSession(boolean hadPreviousSession) {
        this.hadPreviousSession = hadPreviousSession;
    }
    public HttpSession getLastSession(){
        return logins.get(this);
    }
}
