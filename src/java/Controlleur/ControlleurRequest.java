/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlleur;

import Entities.CategorieCyEntity;
import Entities.CategorieEntity;
import Entities.FaceValueEntity;
import Entities.InfoBeneficiaireEntity;
import Entities.ListFaceValueEntity;
import Entities.PaysDeviseEntity;
import Entities.PaysEntity;
import Entities.PaysRequestEntity;
import Entities.PrixEntity;
import Entities.ProduitCyEntity;
import Entities.ProduitEntity;
import RequestResponseEntities.EstimationRequest;
import RequestResponseEntities.EstimationResponse;
import RequestResponseEntities.GetFaceValueResponse;
import RequestResponseEntities.GetPaysResponse;
import RequestResponseEntities.ListProduitCategorieResponse;
import RequestResponseEntities.SendOrderRequest;
import RequestResponseEntities.SendOrderResponse;
import cfcdatabase.Model.Agent;
import cfcdatabase.Model.Categoriecy;
import cfcdatabase.Model.Devise;
import cfcdatabase.Model.Facevaluecy;
import cfcdatabase.Model.Fraisproduitcy;
import cfcdatabase.Model.Infobeneficiaire;
import cfcdatabase.Model.Pays;
import cfcdatabase.Model.Produitcy;
import cfcdatabase.Model.Transactioncy;
import daomanagers.ControleurCategoriecy;
import daomanagers.ControleurDevise;
import daomanagers.ControleurFacevalue;
import daomanagers.ControleurFraisProduitcy;
import daomanagers.ControleurInfobeneficiaire;
import daomanagers.ControleurPaysCategorie;
import daomanagers.ControleurPaysDevise;
import daomanagers.ControleurProduit;
import daomanagers.ControleurProduitcy;
import daomanagers.ControleurTransactionCy;
import daomanagers.ControleurTypeTransaction;
import daomanagers.ControlleurAgent;
import daomanagers.ControlleurCompteMwallet;
import daomanagers.ControlleurForex;
import daomanagers.ControlleurPays;
import daomanagers.DaoManagerCommissionV2;
import entities.CommissionEntity;
import entities.ProduitCategorieCy;
import externApi.CysendApi;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import utility.ErrorResponse;
import utility.HttpDataResponse;
import utility.Utils;

/**
 *
 * @author g.mbuyi
 */
public class ControlleurRequest {
    
    private CysendApi cysendApi;

    public ControlleurRequest() throws Exception {
        
        System.out.println("---------RECUPERATION DES HABILITATION CONFIG CYSEND");
        cysendApi = null;
        try {
            cysendApi = new CysendApi();
        } catch (Exception e) {
        }
        if(cysendApi==null)
        {
            throw new Exception("ECHEC L'OR DE LA RECUPERATION DES DONNEES HABILITAIONS CONFIG");
        }
    }
    
    public Map<String,PaysEntity> getPaysFromCysend()
    {
        Map<String,PaysEntity> map = new TreeMap<>();
        
        try {
            HttpDataResponse<JSONObject> res = cysendApi.getPays();
            
            if(res.getError().getCode().equals("200"))
            {
                JSONArray js = res.getResponse().getJSONArray("js");
                
                for(int i = 0;i<js.length();++i)
                {
                    JSONObject jsP = js.getJSONObject(i);
                    PaysEntity p = PaysEntity.getInstanceFromJson(jsP.toString());
                    map.put(p.getCountry_iso_alpha3(), p);
                            
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return map;
    }
    
    public Map<Long,CategorieEntity> getCategorieFromCysend()
    {
        Map<Long,CategorieEntity> map = new TreeMap<>();
        
        try {
            HttpDataResponse<JSONObject> res = cysendApi.getCategorie();
            
            if(res.getError().getCode().equals("200"))
            {
                JSONArray js = res.getResponse().getJSONArray("js");
                
                for(int i = 0;i<js.length();++i)
                {
                    JSONObject jsP = js.getJSONObject(i);
                    CategorieEntity p = CategorieEntity.getInstanceFromJson(jsP.toString());
                    map.put(new Long(p.getCategory_id()), p);
                            
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return map;
        
    }
    
    public Map<Long,ProduitEntity> getProduitFromCysend()
    {
        Map<Long,ProduitEntity> map = new TreeMap<>();
        try {
            HttpDataResponse<JSONObject> res = cysendApi.getProduit();
            if(res.getError().getCode().equals("200"))
            {
                JSONArray js = res.getResponse().getJSONArray("js");
                
                for(int i = 0;i<js.length();++i)
                {
                    JSONObject jsP = js.getJSONObject(i);
                    ProduitEntity p = ProduitEntity.getInstanceFromJson(jsP.toString());
                    map.put(new Long(p.getProduct_id()), p);
                            
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
    
    public ListFaceValueEntity getFacevalueFromCysend()
    {
        ListFaceValueEntity list = new ListFaceValueEntity();
        
        try {
            HttpDataResponse<JSONObject> res = cysendApi.getFacevalue();
            
            if(res.getError().getCode().equals("200"))
            {
                JSONObject jsOb = res.getResponse().getJSONObject("js");
                JSONArray jsRange = jsOb.getJSONArray("range");
                JSONArray jsFixed = jsOb.getJSONArray("fixed");
                
                System.out.println("---------taille list Facevalue Range : "+jsRange.length());
                System.out.println("---------taille list Facevalue Fixed : "+jsFixed.length());
                
                for(int i = 0 ; i< jsFixed.length();++i)
                {
                    JSONObject js = jsFixed.getJSONObject(i);
                    FaceValueEntity fv = FaceValueEntity.getInstanceFromJson(js.toString());
                    list.getListFixed().put(new Long(fv.getFace_value_id()), fv);
                }
                
                for(int i = 0 ; i< jsRange.length();++i)
                {
                    JSONObject js = jsRange.getJSONObject(i);
                    FaceValueEntity fv = FaceValueEntity.getInstanceFromJson(js.toString());
                    list.getListRange().put(new Long(fv.getFace_value_id()), fv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("---------taille list Facevalue Fixed Renvoyé : "+list.getListFixed().size());
        System.out.println("---------taille list Facevalue Range Renvoyé : "+list.getListRange().size());
        
        return list;
    }
    
    
   public HttpDataResponse<GetPaysResponse> getListPaysFromDb()
   {
       HttpDataResponse<GetPaysResponse> res = new HttpDataResponse<>();
       List<PaysRequestEntity> list = new ArrayList<>();
       ControlleurPays ctrl = new ControlleurPays();
       
       try {
           Map<Long,Pays> map = ctrl.getPaysIdCysend();
           
           for(Long l : map.keySet())
           {
               Pays p = map.get(l);
               PaysRequestEntity pays = new PaysRequestEntity(p.getIdpays(), p.getNom(), p.getIdcysend(),p.getCodeiso3(),p.getIsocountrycd());
               
               list.add(pays);
           }
           
           res.setError(new ErrorResponse("200", "OK"));
           GetPaysResponse gpr = new GetPaysResponse(list);
           res.setResponse(gpr);
           
       } catch (Exception e) {
           e.printStackTrace();
       }
       
       return res;
   }
   
   public HttpDataResponse<GetPaysResponse> getListPaysTelcoFromDb()
   {
       HttpDataResponse<GetPaysResponse> res = new HttpDataResponse<>();
       List<PaysRequestEntity> list = new ArrayList<>();
       ControlleurPays ctrl = new ControlleurPays();
       
       try {
           Map<String,Pays> map = ctrl.ToMapPays(ctrl.getListCysendTelco());
           
           for(String l : map.keySet())
           {
               Pays p = map.get(l);
               PaysRequestEntity pays = new PaysRequestEntity(p.getIdpays(), p.getNom(), p.getIdcysend()
                       ,p.getCodeiso3(),p.getIsocountrycd(),p.getPrefix());
               
               list.add(pays);
           }
           
           res.setError(new ErrorResponse("200", "OK"));
           GetPaysResponse gpr = new GetPaysResponse(list);
           res.setResponse(gpr);
           
       } catch (Exception e) {
           e.printStackTrace();
       }
       
       return res;
   }
   
   public HttpDataResponse<List<ProduitCyEntity>> GetListProduitByPays(String codeiso , String nom)
   {
       List<ProduitCyEntity> list = new ArrayList<>();
       HttpDataResponse<List<ProduitCyEntity>> res = new HttpDataResponse<>();
       
       ControleurProduitcy cp = new ControleurProduitcy();
       
       nom = nom.toUpperCase();
       
       List<Produitcy> ls = cp.getListByPays(codeiso,nom);
       
       for(Produitcy p : ls)
       {
           ProduitCyEntity cpe = new ProduitCyEntity();
           cpe.setIdProduit(p.getId());
           cpe.setLogo(p.getLogo());
           cpe.setMaintenance(p.getMaintenance());
           cpe.setNom(p.getProductname());
           cpe.setTypeproduit(p.getTypeproduit());
           
           list.add(cpe);
       }
       
       res.setError(new ErrorResponse("200", "OK"));
       res.setResponse(list);
       
       
       return res;
   }
   
   public ListProduitCategorieResponse GetProduit(String codeiso3)
   {
       ListProduitCategorieResponse res= new ListProduitCategorieResponse();
       
       ControleurPaysCategorie ctrl = new ControleurPaysCategorie();
       
       List<ProduitCategorieCy> list = ctrl.getListCatProd(codeiso3);
       
       Map<String,CategorieCyEntity> mapCatEnt = new TreeMap<>();
       
       
       for(ProduitCategorieCy p : list)
       {
           CategorieCyEntity cat = new CategorieCyEntity();
           if(mapCatEnt.containsKey(p.getNOMCATEGORIE()))
           {
               cat = mapCatEnt.get(p.getNOMCATEGORIE());
           }
           else
           {
               cat.setIdCategorie(p.getIDCATEGORIE());
               cat.setNom(p.getNOMCATEGORIE());
           }
           
           ProduitCyEntity prod = new ProduitCyEntity();
           prod.setIdProduit(p.getIDPRODUIT());
           prod.setLogo(p.getLOGO());
           prod.setMaintenance(p.getMAINTENANCE());
           prod.setNom(p.getNOMPRODUIT());
           prod.setTypeproduit(p.getTYPEPRODUIT());
           
           cat.getListProduit().add(prod);
           mapCatEnt.put(p.getNOMCATEGORIE(), cat);
           
       }
       
       List<CategorieCyEntity> listCat = new ArrayList<>();
       for(String nom : mapCatEnt.keySet())
       {
           listCat.add(mapCatEnt.get(nom));
       }
       
       res.setListCat(listCat);
       
       return res;
   }
   
   public ListProduitCategorieResponse GetCategorie(String codeiso3)
   {
       ListProduitCategorieResponse res= new ListProduitCategorieResponse();
       
       ControleurPaysCategorie ctrl = new ControleurPaysCategorie();
       
       List<ProduitCategorieCy> list = ctrl.getListCatProd(codeiso3);
       
       Map<String,CategorieCyEntity> mapCatEnt = new TreeMap<>();
       
       
       for(ProduitCategorieCy p : list)
       {
           CategorieCyEntity cat = new CategorieCyEntity();
           if(mapCatEnt.containsKey(p.getNOMCATEGORIE()))
           {
               cat = mapCatEnt.get(p.getNOMCATEGORIE());
           }
           else
           {
               cat.setIdCategorie(p.getIDCATEGORIE());
               cat.setNom(p.getNOMCATEGORIE());
           }
           
           mapCatEnt.put(p.getNOMCATEGORIE(), cat);
           
       }
       
       List<CategorieCyEntity> listCat = new ArrayList<>();
       for(String nom : mapCatEnt.keySet())
       {
           listCat.add(mapCatEnt.get(nom));
       }
       
       res.setListCat(listCat);
       
       return res;
   }
   
   public ListProduitCategorieResponse GetListProduitByCategorie(Long idCategorie , String codeIso3)
   {
       ListProduitCategorieResponse res= new ListProduitCategorieResponse();
       
       ControleurPaysCategorie ctrl = new ControleurPaysCategorie();
       
       List<ProduitCategorieCy> list = ctrl.getListProdByCat(idCategorie , codeIso3);
       
       Map<String,CategorieCyEntity> mapCatEnt = new TreeMap<>();
       
       
       for(ProduitCategorieCy p : list)
       {
           CategorieCyEntity cat = new CategorieCyEntity();
           if(mapCatEnt.containsKey(p.getNOMCATEGORIE()))
           {
               cat = mapCatEnt.get(p.getNOMCATEGORIE());
           }
           else
           {
               cat.setIdCategorie(p.getIDCATEGORIE());
               cat.setNom(p.getNOMCATEGORIE());
           }
           
           ProduitCyEntity prod = new ProduitCyEntity();
           prod.setIdProduit(p.getIDPRODUIT());
           prod.setLogo(p.getLOGO());
           prod.setMaintenance(p.getMAINTENANCE());
           prod.setNom(p.getNOMPRODUIT());
           prod.setTypeproduit(p.getTYPEPRODUIT());
           
           cat.getListProduit().add(prod);
           mapCatEnt.put(p.getNOMCATEGORIE(), cat);
           
       }
       
       List<CategorieCyEntity> listCat = new ArrayList<>();
       for(String nom : mapCatEnt.keySet())
       {
           listCat.add(mapCatEnt.get(nom));
       }
       
       res.setListCat(listCat);
       
       return res;
   }
   
   public GetFaceValueResponse getPrix(Long idProduit)
   {
       GetFaceValueResponse res = new GetFaceValueResponse();
       ControleurFacevalue ctrl = new ControleurFacevalue();
       
       List<Facevaluecy> list = ctrl.getFacevalueByProduit(idProduit);
       List<PrixEntity> listPrix = new PrixEntity().getList(list);
       
       res.setPrix(listPrix);
       
       
       return res;
   }
    
    
    public SendOrderResponse sendTransaction(Transactioncy tr ,Facevaluecy face,Double step, List<Infobeneficiaire> listInfo)
    {
        SendOrderResponse res = new SendOrderResponse();
        
        String userUid ="FLASH_"+cysendApi.getScenario()+"_"+tr.getId();
        Double facevalue = step;
        if(face.getTypefacevalue().equalsIgnoreCase("FIXED"))
            facevalue = face.getFacevalue();
        
        HttpDataResponse<JSONObject> result = cysendApi.postOrder(userUid, face.getFacevalueid(), facevalue,
                face.getFacevaluecurrency(), listInfo);
        
        
        return res;
    }
    
    public PaysDeviseEntity getDevise(Long idAgent)
    {
        PaysDeviseEntity pd = new PaysDeviseEntity();
        ControlleurAgent ctrlAgent = new ControlleurAgent();
        Agent ag = ctrlAgent.getAgent(new Long(idAgent));
        ControleurPaysDevise ctrlPaysDevise = new ControleurPaysDevise();
        Long idPays = ag.getAgence().getAdresse().getVille().getRegion().getPays().getIdpays();
        List<String> listCodeIso = ctrlPaysDevise.getListCodeIso(idPays);
        pd.setListDevise(listCodeIso);
        
        return pd;
        
    }
    
    public Long getCategorieTelco()
    {
        Long l = null;
        
        ControleurCategoriecy ctrl = new ControleurCategoriecy();
        Categoriecy cat = ctrl.getCategoriTelco();
        if(cat!=null)
        {
            l = cat.getId();
        }
        
        return l;
    }
}
