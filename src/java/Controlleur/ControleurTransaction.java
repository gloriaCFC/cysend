/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlleur;

import Entities.InfoBeneficiaireEntity;
import Entities.PrepaidCode;
import Entities.TransactionEntity;
import RequestResponseEntities.EstimationRequest;
import RequestResponseEntities.EstimationResponse;
import RequestResponseEntities.ForceCallbackRequest;
import RequestResponseEntities.ListTransactionRequest;
import RequestResponseEntities.SendOrderRequest;
import RequestResponseEntities.SendOrderResponse;
import RequestResponseEntities.SendOrderTelcoRequest;
import cfcdatabase.Model.Agent;
import cfcdatabase.Model.Devise;
import cfcdatabase.Model.Facevaluecy;
import cfcdatabase.Model.Fraisproduit;
import cfcdatabase.Model.Fraisproduitcy;
import cfcdatabase.Model.Infobeneficiaire;
import cfcdatabase.Model.Pays;
import cfcdatabase.Model.Produit;
import cfcdatabase.Model.Transactioncy;
import controller.CommissionementBlockChain;
import controller.ControlleurBonusAgent;
import daomanagers.ControleurDevise;
import daomanagers.ControleurFacevalue;
import daomanagers.ControleurFraisProduitcy;
import daomanagers.ControleurInfobeneficiaire;
import daomanagers.ControleurMail;
import daomanagers.ControleurProduit;
import daomanagers.ControleurProduitpayscy;
import daomanagers.ControleurTransactionCy;
import daomanagers.ControleurTypeTransaction;
import daomanagers.ControlleurAgent;
import daomanagers.ControlleurApplication;
import daomanagers.ControlleurCompteMwallet;
import daomanagers.ControlleurForex;
import daomanagers.ControlleurPays;
import daomanagers.DaoManagerCommissionV2;
import daomanagers.DaoManagerFraisProduit;
import entities.CommissionEntity;
import externApi.CysendApi;
import externApi.DaoEnvoiSms;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import utility.ErrorResponse;
import utility.HttpDataResponse;
import utility.MailUtils;
import utility.Utils;

/**
 *
 * @author g.mbuyi
 */
public class ControleurTransaction {
    
    private CysendApi cysendApi;

    public ControleurTransaction() throws Exception {
        
        System.out.println("---------RECUPERATION DES HABILITATION CONFIG CYSEND");
        cysendApi = null;
        try {
            cysendApi = new CysendApi();
        } catch (Exception e) {
        }
        
        if(cysendApi==null)
        {
            throw new Exception("ECHEC L'OR DE LA RECUPERATION DES DONNEES HABILITAIONS CONFIG");
        }
    }
    
    
    
    
    public HttpDataResponse<EstimationResponse> getEstimation(EstimationRequest re , Agent agent)
    {
        HttpDataResponse<EstimationResponse> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500", "KO");
        
        boolean ok = true;
        
        try {
            Long idFacevalue = re.getIdFacevalue();
            ControleurFacevalue ctrlFaceValue = new ControleurFacevalue();
            ControleurInfobeneficiaire ctrlInfo = new ControleurInfobeneficiaire();
            Facevaluecy face = ctrlFaceValue.getFacevalueById(idFacevalue);
            ControleurFraisProduitcy ctrlFrais = new ControleurFraisProduitcy();
            DaoManagerFraisProduit ctrlFraisProd = new DaoManagerFraisProduit();
            Double frais = ctrlFrais.getFraisParDefaut().getFrais();
            Double fraisSms = 0d;
            Fraisproduitcy fraisProd = ctrlFrais.getFraisByIdProduit(face.getProduitcy().getId());
            
            ControlleurForex cForex = new ControlleurForex();
            ControleurProduit cp = new ControleurProduit();
            Long idp = cp.getIdProduit(ControleurProduit.PRODUIT_CYSEND);
            
            if(fraisProd!=null)
            {
                frais = fraisProd.getFrais();
            }
            
            String typeFace = face.getTypefacevalue();
            
            Double montant = 0d;
            
            if(typeFace.equalsIgnoreCase("FIXED"))
            {
                montant = face.getCost();
                
            }
            else
            {
                
                Double step = re.getStep();
                if(step!= null)
                {
                    System.out.println("step non null");
                    if(step>=face.getFacevaluefrom() & step<= face.getFacevalueto())
                    {
                        montant = (face.getMaximumcost()/face.getFacevalueto())*step;
                        if(montant > face.getMaximumcost())
                        {
                            montant = face.getMaximumcost();
                        }
                        else if(montant < face.getMinimumcost())
                        {
                            montant = face.getMinimumcost();
                        }
                    }
                    else
                    {
                        ok = false;
                        error.setCodeAndDescription("400", "STEP INCORRECT ( "+re.getStep()+" ) , l'intervalle est de "
                                + ": ["+face.getFacevaluefrom()+" , "+face.getFacevalueto()+"]");
                        res.setError(error);
                    }
                }
                else
                {
                    ok = false;
                    error.setCodeAndDescription("400", "STEP MANQUAND");
                    res.setError(error);
                }
                
            }
            
            System.out.println("notification : "+re.getNotification());
            if(re.getNotification().equalsIgnoreCase("true"))
            {
                
                Fraisproduit f = ctrlFraisProd.getV2(ControleurProduit.PRODUIT_CYSEND, "SERVICE");
                fraisSms = f.getMontant();
                
                if(fraisSms==null)
                {
                    ok = false;
                    error.setCodeAndDescription("400", "AUCUN FRAIS SMS PARAMETRE POUR CE PRODUIT");
                    res.setError(error);
                }
                else
                {
                    System.out.println("get montant forex frais sms cysend : "+fraisSms+" , idprod : "+idp);
                    fraisSms = cForex.getMontantForex("USD", re.getDeviseTransaction(), fraisSms, idp);
                }
            }
            
            if(frais==null)
            {
                ok = false;
                error.setCodeAndDescription("400", "AUCUN FRAIS PARAMETRE PAR DEFAUT ET / OU POUR CE PRODUIT");
                res.setError(error);
            }
            
            if(ok == true)
            {
                
                System.out.println("montant avant Forex : "+montant);

                Double montantForex = montant;

                if(!re.getDeviseTransaction().equalsIgnoreCase("USD"))
                {
                    montantForex = cForex.getMontantForex("USD", re.getDeviseTransaction(), montant, idp);
                    
                }

                System.out.println("montant après Forex : "+montantForex);
                
                
                System.out.println("---------Type de Facevalue : "+typeFace);
                System.out.println("montant Cysend : "+montant);
                System.out.println("montant transaction : "+montantForex);
                System.out.println("pourcentage frais : "+frais);

                Double montantFrais = (montantForex*frais)/100;
                System.out.println("Frais à appliquer : "+montantFrais);
                Double montantForexAvecFrais = montantForex + montantFrais;
                montantForexAvecFrais +=fraisSms;

                

                EstimationResponse est = new EstimationResponse();
                est.setDeviseTransaction(re.getDeviseTransaction());
                est.setIdFacevalue(idFacevalue);
                est.setMontant(montantForex);
                est.setMontantTotal(montantForexAvecFrais);
                est.setFrais(montantFrais);
                est.setFraisSms(fraisSms);
                
                if(typeFace.equalsIgnoreCase("FIXED"))
                {
                    est.setFacevalueChoisi(face.getFacevalue());
                }
                else
                {
                    est.setFacevalueChoisi(re.getStep());
                }
                

                List<InfoBeneficiaireEntity> listInfoBen = new ArrayList<>();

                List<Infobeneficiaire> listInfo = ctrlInfo.getList(face.getProduitcy().getId());
                for(Infobeneficiaire i : listInfo)
                {
                    InfoBeneficiaireEntity in = new InfoBeneficiaireEntity();
                    in.setDescription(i.getDescription());
                    in.setName(i.getNom());
                    in.setRequired(new Boolean(i.getRequired()));
                    in.setType(i.getType());
                    in.setPattern(i.getPattern().replace("/", ""));

                    listInfoBen.add(in);
                }

                est.setListInfoBene(listInfoBen);

                res.setResponse(est);
                error.setCodeAndDescription("200", "OK");
                res.setError(error);
            }
            
        } catch (Exception e) {
            error.setCodeAndDescription("500", e.toString());
            e.printStackTrace();
        }
        
        return res;
    }
    
    
    public HttpDataResponse<SendOrderResponse> sendOrder(SendOrderRequest re , Agent agentTerminal)
    {
        HttpDataResponse<SendOrderResponse> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500", "KO");
        SendOrderResponse resObj = new SendOrderResponse();
        ControlleurAgent ctrlAgent = new ControlleurAgent();
        
        Long idAgent = re.getIdAgentRemote();
        Agent agent= agentTerminal;
        if(idAgent!=null)
        {
            agent = ctrlAgent.getAgent(idAgent);
        }
        
        ControleurInfobeneficiaire ctrlInfo = new ControleurInfobeneficiaire();
        ControleurFacevalue ctrlFaceValue = new ControleurFacevalue();
        Facevaluecy face = ctrlFaceValue.getFacevalueById(re.getIdFacevalue());
        
        //Get estimation pour checquer le solde
        
        EstimationRequest estReq = new EstimationRequest();
        estReq.setDeviseTransaction(re.getDeviseTransaction());
        estReq.setIdFacevalue(re.getIdFacevalue());
        estReq.setStep(re.getStep());
        estReq.setNotification(re.getNotification());
        
        HttpDataResponse<EstimationResponse> estimation = this.getEstimation(estReq, agent);
        EstimationResponse estResp = estimation.getResponse();
        String deviseT = re.getDeviseTransaction();
        String infoBenToSave = "";
        
        if(estimation.getError().getCode().equalsIgnoreCase("200"))
        {    
        
            //CHECK SOLDE

            System.out.println("****************************Check de solde****************************");
            ControlleurCompteMwallet ccm = new ControlleurCompteMwallet();

            Boolean soldeOk = ccm.isEnoughBalanceOrIsCfcAgent(agent, deviseT,estResp.getMontantTotal());
            if(soldeOk==false)
            { 
                System.out.println("---------------SOLDE INSUFFISANT");
                error.setCodeAndDescription("400", "SOLDE INSUFFISANT");
            }
            else
            {

                System.out.println("***********CHECK INFO BENEFICIAIRE************");

                Map<String,Infobeneficiaire> listInfo = ctrlInfo.TopMapList(ctrlInfo.getList(face.getProduitcy().getId())) ;
                Map<String, InfoBeneficiaireEntity> listInfoBenReq = new InfoBeneficiaireEntity().TomapList(re.getListInfoBene());
                List<Infobeneficiaire> listInfoToSend = new ArrayList<>();
                Boolean checkInfo = true;

                for(String key : listInfo.keySet())
                {
                    String pattern = listInfo.get(key).getPattern().replace("/", "");
                   if(listInfoBenReq.containsKey(key))
                   {
                       InfoBeneficiaireEntity ifR = listInfoBenReq.get(key);
                       if(!Pattern.matches(pattern, ifR.getValue()))
                       {
                           checkInfo = false;
                           break;
                       }
                       else
                       {
                           Infobeneficiaire i = new Infobeneficiaire();
                           i.setNom(ifR.getName());
                           i.setValue(ifR.getValue());
                           listInfoToSend.add(i);
                           infoBenToSave +=ifR.getName()+"_"+ifR.getValue()+";";
                       }
                   }
                   else
                   {
                       checkInfo = false;
                       break;
                   }
                }
                
                if(re.getNotification().equalsIgnoreCase("true")&&re.getNumPhoneNotif()==null)
                {
                    checkInfo = false;
                }


                if(checkInfo == true)
                {
                    /**************GET COMMISSION************/
                    try {
                        CommissionEntity commissionEntity = new DaoManagerCommissionV2().getCommissionEntity(agent.getAgence().getCodeagence(), ControleurProduit.PRODUIT_CYSEND
                                                                                           , ControleurTypeTransaction.TYPE_SERVICE);
            
                    
                        Double commission = (commissionEntity.getCommission()*estResp.getMontantTotal())/100;
                        Double commissionFCN = 0d;
                        
                        if(deviseT.equalsIgnoreCase("FCN"))
                        {
                            commissionFCN = (commissionEntity.getCommissionFcn()*estResp.getMontantTotal())/100;
                        }
                        else
                        {
                            ControlleurForex cForex = new ControlleurForex();
                            Long idproduit = new daomanagers.ControleurProduit().getIdProduit(daomanagers.ControleurProduit.PRODUIT_CYSEND);
                            Double montantFCN = cForex.getMontantForex(re.getDeviseTransaction(), "FCN", estResp.getMontantTotal(), idproduit);
                            
                            commissionFCN = (montantFCN * commissionEntity.getCommissionFcn())/100;
                        }
                            
                    
                        /************CREATION TRANSACTION CYSEND ***********/
                        ControleurDevise ctrlDevise = new ControleurDevise();
                        Devise dev = ctrlDevise.getDeviseWithIso(deviseT);
                        Transactioncy transaction = new Transactioncy();
                        transaction.setAgence(agent.getAgence());
                        transaction.setAgentByAgent(agent);
                        transaction.setAgentByAgentremote(agentTerminal);
                        transaction.setDate(new Date());
                        transaction.setDevise(dev);
                        transaction.setFacevaluecy(face);
                        transaction.setFrais(estResp.getFrais());
                        transaction.setMontant(estResp.getMontant());
                        transaction.setStatut(Utils.STATUT_PENDING);
                        transaction.setCommission(commission);
                        transaction.setCommissionfcn(commissionFCN);
                        transaction.setExtra1(infoBenToSave);
                        String infoNotification = re.getEmailNotif()+"_"+re.getNumPhoneNotif()+";";
                        
                        transaction.setExtra3(infoNotification);
                        
                        ControleurTransactionCy ctrlTrans = new ControleurTransactionCy();
                        Transactioncy tSave = ctrlTrans.save(transaction);
                        
                        if(tSave!=null)
                        {
                            /**********SEND TRANSACTION TO CYSEND***********/
                            
                            HttpDataResponse<JSONObject> resOrder = this.cysendApi.postOrder(String.valueOf(tSave.getId()), 
                                    face.getFacevalueid(), estimation.getResponse().getFacevalueChoisi(),
                                    face.getFacevaluecurrency(), listInfoToSend);
                            
                            if(resOrder.getError().getCode().equalsIgnoreCase("200") ||
                                resOrder.getError().getCode().equalsIgnoreCase("201")     )
                            {
                                JSONArray jsAr = resOrder.getResponse().getJSONArray("js");
                                JSONObject js = jsAr.getJSONObject(0);
                                SendOrderResponse response = SendOrderResponse.getInstanceFromJson(js.toString());
                                String cysendStatus = response.getStatus();
                                PrepaidCode prepaidCode = response.getPrepaid_code();
                                
                                if(prepaidCode != null)
                                {
                                    tSave.setCode(prepaidCode.getCode());
                                    tSave.setSerial(prepaidCode.getSerial());
                                    tSave.setExpirationdate(prepaidCode.getExpiration());
                                }
                                
                                tSave.setIdexterne(response.getUid());
                                tSave.setIssuerreference(response.getIssuer_reference());
                                tSave.setResponsecode(response.getResponse_code());
                                tSave.setResponsedescription(response.getResponse_description());
                                tSave.setResponsetitle(response.getResponse_title());
                                tSave.setUseruid(response.getUser_uid());
                                
                                tSave.setCystatus(cysendStatus);
                                
                                if(cysendStatus.equalsIgnoreCase("success"))
                                {
                                    tSave.setStatut("SUCCESS");
                                    error.setCodeAndDescription("200", "SUCCESS");
                                    
                                    // ***************************consommation cash in du pos*************************
                                    CommissionementBlockChain cbc = new CommissionementBlockChain();
                                    try{
                                        System.out.println("-------------------Consommation pour le pos qui effectue un abonnement BLEUSAT : "+agent.getLogin());
                                        cbc.passerConsommationWeb(agent.getLogin(), "SERVICE", ControlleurApplication.APPLICATION_CYSEND,
                                                tSave.getMontant(), dev.getCodeiso(), tSave.getId());
                                    }catch(Exception ex){
                                        System.out.println("---------------------------------- CYSEND : "+ex.getMessage());
                                        System.out.println("-------------------------ECHEC DE LA CONSOMMATION---------------");
                                        ex.printStackTrace();
                                        //throw new CustomException("500", "Le transfer a abouti, cependant une erreur s'est produite durant le calcul des commissions");
                                    }
                                    
                                    /*****************BONUS***************/
                                             
                                    ControlleurBonusAgent ctrlBonus = new ControlleurBonusAgent();
                                    ControleurDevise ctrlD = new ControleurDevise();
                                    ControleurProduit controleurProduit = new ControleurProduit();
                                    Produit p = controleurProduit.getProduit(daomanagers.ControleurProduit.PRODUIT_CYSEND);
                                    
                                    ctrlBonus.sendBonusAgent(agent, daomanagers.ControleurProduit.PRODUIT_CYSEND, p.getIdproduit(),
                                            tSave.getMontant(), dev, tSave.getId());
                                    
                                }
                                else if(cysendStatus.equalsIgnoreCase("accepted"))
                                {
                                    tSave.setStatut("EN COURS");
                                    error.setCodeAndDescription("200", "EN COURS");
                                }
                                else
                                {
                                    tSave.setStatut("FAILED");
                                    error.setCodeAndDescription("400", tSave.getResponsedescription());
                                }
                                
                                
                                res.setResponse(response);
                            }
                            else
                            {
                                JSONObject js = resOrder.getResponse().getJSONObject("js");
                                tSave.setResponsecode(js.getString("code"));
                                tSave.setResponsedescription(js.getString("message"));
                                tSave.setResponsetitle(js.getString("information"));
                                
                                tSave.setStatut("FAILED");
                                
                                error.setCodeAndDescription("400", tSave.getResponsedescription());
                            }
                            
                           ctrlTrans.save(tSave);
                        }
                        else
                        {
                            error.setCodeAndDescription("400", "ERREUR LORS DE L'ENREGISTREMENT DE LA TRANSACTION");
                            System.out.println("ECHEC TRANSACTION : "+error);
                        }
                        
                        if(tSave.getCode()!=null)
                        {
                            String sms ="carte : "+tSave.getCode()+" , serie : "+tSave.getSerial()+" , dat exp : "+tSave.getExpirationdate();                               
                          
                            if(re.getNumPhoneNotif()!=null && re.getNotification().equalsIgnoreCase("true"))
                            {
                                DaoEnvoiSms daoSms = new DaoEnvoiSms();
                                 daoSms.bulkSmsV1(sms, re.getNumPhoneNotif(), "");
                            }
                            
                            if(re.getEmailNotif()!=null)
                            {
                                String produit= "Nom produit";
                                List<String> lsEmail = new ArrayList<>();
                                lsEmail.add(re.getEmailNotif());
                                ControleurMail ctrlMail = new ControleurMail();
                                ctrlMail.processMail(MailUtils.ADRESSE_FLASHPOS, MailUtils.PASSWORD_FLASHPOS, "CODE "+produit, sms, lsEmail);
                            }
                        }
                        
                    
                    } catch (Exception e) {
                        e.printStackTrace();
                        error.setCodeAndDescription("400", e.toString());
                    }
                    
                }
                else
                {
                    error.setCodeAndDescription("400", "INFO BENEFICIAIRE INCORRECT ET / OU NUMERO NOTIFICATION INCORRECT");
                    System.out.println("ECHEC TRANSACTION : "+error);
                }


            }
        }
        else
        {
            
            error = estimation.getError();
            System.out.println("ECHEC TRANSACTION : "+error);
        }
        
        res.setError(error);
        
        
        return res;
    }
    
    public HttpDataResponse<List<TransactionEntity>> getListTransaction(ListTransactionRequest re ,boolean isUserRestricted, Agent agent )
    {
        HttpDataResponse<List<TransactionEntity>> response = new HttpDataResponse<>();
        ControleurTransactionCy ctrl = new ControleurTransactionCy();
        List<TransactionEntity> listTrans = new ArrayList<>();
        
        ErrorResponse er = new ErrorResponse("500", "ECHEC");
        
        String codeagence = isUserRestricted ? agent.getAgence().getCodeagence():re.getCodeagence();
        Long idagent      = isUserRestricted ? agent.getIdagent() : null;
        
        try {
            List<JSONObject> list = ctrl.getList(re.getDate0(), re.getDate1(), idagent, re.getLogin(), codeagence,re.getIdville(), 
                re.getIdregion(), re.getIdpays(), re.getPosid(), re.getIddevise(), re.getDeviseiso(), re.getStatut());
        
            for(JSONObject j : list)
            {
                TransactionEntity tr = TransactionEntity.getInstanceFromJson(j.toString());
                listTrans.add(tr);

            }
            response.setResponse(listTrans);
            er.setCodeAndDescription("200", "OK");
        } catch (Exception e) {
            er.setCodeAndDescription("500", e.toString());
        }
        
        response.setError(er);
        
        return response;
    }
    
    
    public HttpDataResponse<String> forcerCallback(ForceCallbackRequest re , Agent agent)
    {
        HttpDataResponse<String> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500","KO");
        
        ControleurTransactionCy ctrlTr = new ControleurTransactionCy();
        Transactioncy tr = ctrlTr.getTransactionById(re.getIdTransaction());
        
        try {
            if(tr.getIdexterne()!=null)
            {
                String uid = tr.getIdexterne();
                String userId = tr.getUseruid();
                
                HttpDataResponse<JSONObject> resF = this.cysendApi.forceCallback(uid, userId);
                
                if(!resF.getError().getCode().equalsIgnoreCase("202"))
                {
                    System.out.println("------------ resF.getError().getCode()");
                    JSONObject js = resF.getResponse().getJSONObject("js");
                    error.setCodeAndDescription("400", js.getString("information"));
                }
                else
                {
                    error = resF.getError();
                }
                
            }
            else
            {
                error.setCodeAndDescription("400", "IMPOSSIBLE DE FORCER LE CALLBACK, IDEXTERNE NULL");
            }
        } catch (Exception e) {
            
            error.setCodeAndDescription("500", e.toString());
            e.printStackTrace();
        }
        
        res.setError(error);
        return res;
    }
    
    
    public HttpDataResponse<SendOrderResponse> sendOrderTelco(SendOrderTelcoRequest re , Agent agentTerminal)
    {
        HttpDataResponse<SendOrderResponse> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500", "KO");
        SendOrderResponse resObj = new SendOrderResponse();
        ControlleurAgent ctrlAgent = new ControlleurAgent();
        
        Long idAgent = re.getIdAgentRemote();
        Agent agent= agentTerminal;
        if(idAgent!=null)
        {
            agent = ctrlAgent.getAgent(idAgent);
        }
        
        ControleurInfobeneficiaire ctrlInfo = new ControleurInfobeneficiaire();
        ControleurFacevalue ctrlFaceValue = new ControleurFacevalue();
        Facevaluecy face = ctrlFaceValue.getFacevalueById(re.getIdFacevalue());
        
        ControlleurPays ctrlP = new ControlleurPays();
        Pays p = ctrlP.get(re.getIdPays());
        String prefixPays = p.getPrefix();
        
        //Get estimation pour checquer le solde
        
        EstimationRequest estReq = new EstimationRequest();
        estReq.setDeviseTransaction(re.getDeviseTransaction());
        estReq.setIdFacevalue(re.getIdFacevalue());
        estReq.setStep(re.getStep());
                
        HttpDataResponse<EstimationResponse> estimation = this.getEstimation(estReq, agent);
        EstimationResponse estResp = estimation.getResponse();
        String deviseT = re.getDeviseTransaction();
        String infoBenToSave = "";
        
        if(estimation.getError().getCode().equalsIgnoreCase("200"))
        {    
        
            //CHECK SOLDE

            System.out.println("****************************Check de solde****************************");
            ControlleurCompteMwallet ccm = new ControlleurCompteMwallet();

            Boolean soldeOk = ccm.isEnoughBalanceOrIsCfcAgent(agent, deviseT,estResp.getMontantTotal());
            if(soldeOk==false)
            { 
                System.out.println("---------------SOLDE INSUFFISANT");
                error.setCodeAndDescription("400", "SOLDE INSUFFISANT");
            }
            else
            {

                System.out.println("***********CHECK INFO BENEFICIAIRE************");
                
                Map<String, InfoBeneficiaireEntity> listInfoBenReq = new InfoBeneficiaireEntity().TomapList(re.getListInfoBene());
                List<Infobeneficiaire> listInfoToSend = new ArrayList<>();
                Boolean checkInfo = true;
                
                if(listInfoBenReq.containsKey("mobile"))
                {
                    InfoBeneficiaireEntity ifR = listInfoBenReq.get("mobile");
                    if(prefixPays!=null)
                    {
                        if(!ifR.getValue().contains("+"+prefixPays))
                        {
                            checkInfo = false;
                        }
                        else
                        {
                           Infobeneficiaire i = new Infobeneficiaire();
                           i.setNom(ifR.getName());
                           i.setValue(ifR.getValue());
                           listInfoToSend.add(i);
                           infoBenToSave +=ifR.getName()+"_"+ifR.getValue()+";";
                        }
                    }
                    else
                    {
                        Infobeneficiaire i = new Infobeneficiaire();
                        i.setNom(ifR.getName());
                        i.setValue(ifR.getValue());
                        listInfoToSend.add(i);
                        infoBenToSave +=ifR.getName()+"_"+ifR.getValue()+";";
                    }
                    
                }
                else
                {
                    checkInfo = false;
                }
                
                
                if(checkInfo == true)
                {
                    /**************GET COMMISSION************/
                    try {
                        CommissionEntity commissionEntity = new DaoManagerCommissionV2().getCommissionEntity(agent.getAgence().getCodeagence(), ControleurProduit.PRODUIT_CYSEND
                                                                                           , ControleurTypeTransaction.TYPE_SERVICE);
            
                    
                        Double commission = (commissionEntity.getCommission()*estResp.getMontantTotal())/100;
                        Double commissionFCN = 0d;
                        
                        if(deviseT.equalsIgnoreCase("FCN"))
                        {
                            commissionFCN = (commissionEntity.getCommissionFcn()*estResp.getMontantTotal())/100;
                        }
                        else
                        {
                            ControlleurForex cForex = new ControlleurForex();
                            Long idproduit = new daomanagers.ControleurProduit().getIdProduit(daomanagers.ControleurProduit.PRODUIT_CYSEND);
                            Double montantFCN = cForex.getMontantForex(re.getDeviseTransaction(), "FCN", estResp.getMontantTotal(), idproduit);
                            
                            commissionFCN = (montantFCN * commissionEntity.getCommissionFcn())/100;
                        }
                            
                    
                        /************CREATION TRANSACTION CYSEND ***********/
                        ControleurDevise ctrlDevise = new ControleurDevise();
                        Devise dev = ctrlDevise.getDeviseWithIso(deviseT);
                        Transactioncy transaction = new Transactioncy();
                        transaction.setAgence(agent.getAgence());
                        transaction.setAgentByAgent(agent);
                        transaction.setAgentByAgentremote(agentTerminal);
                        transaction.setDate(new Date());
                        transaction.setDevise(dev);
                        transaction.setFacevaluecy(face);
                        transaction.setFrais(estResp.getFrais());
                        transaction.setMontant(estResp.getMontant());
                        transaction.setStatut(Utils.STATUT_PENDING);
                        transaction.setCommission(commission);
                        transaction.setCommissionfcn(commissionFCN);
                        transaction.setExtra1(infoBenToSave);
                        transaction.setExtra2("TELCO");
                        
                        ControleurTransactionCy ctrlTrans = new ControleurTransactionCy();
                        Transactioncy tSave = ctrlTrans.save(transaction);
                        
                        if(tSave!=null)
                        {
                            /**********SEND TRANSACTION TO CYSEND***********/
                            
                            HttpDataResponse<JSONObject> resOrder = this.cysendApi.postOrder(String.valueOf(tSave.getId()), 
                                    face.getFacevalueid(), estimation.getResponse().getFacevalueChoisi(),
                                    face.getFacevaluecurrency(), listInfoToSend);
                            
                            if(resOrder.getError().getCode().equalsIgnoreCase("200") ||
                                resOrder.getError().getCode().equalsIgnoreCase("201")     )
                            {
                                JSONArray jsAr = resOrder.getResponse().getJSONArray("js");
                                JSONObject js = jsAr.getJSONObject(0);
                                SendOrderResponse response = SendOrderResponse.getInstanceFromJson(js.toString());
                                String cysendStatus = response.getStatus();
                                PrepaidCode prepaidCode = response.getPrepaid_code();
                                
                                if(prepaidCode != null)
                                {
                                    tSave.setCode(prepaidCode.getCode());
                                    tSave.setSerial(prepaidCode.getSerial());
                                    tSave.setExpirationdate(prepaidCode.getExpiration());
                                }
                                
                                tSave.setIdexterne(response.getUid());
                                tSave.setIssuerreference(response.getIssuer_reference());
                                tSave.setResponsecode(response.getResponse_code());
                                tSave.setResponsedescription(response.getResponse_description());
                                tSave.setResponsetitle(response.getResponse_title());
                                tSave.setUseruid(response.getUser_uid());
                                
                                tSave.setCystatus(cysendStatus);
                                
                                if(cysendStatus.equalsIgnoreCase("success"))
                                {
                                    tSave.setStatut("SUCCESS");
                                    error.setCodeAndDescription("200", "SUCCESS");
                                    
                                     // ***************************consommation cash in du pos*************************
                                    CommissionementBlockChain cbc = new CommissionementBlockChain();
                                    try{
                                        System.out.println("-------------------Consommation pour le pos qui effectue un abonnement BLEUSAT : "+agent.getLogin());
                                        cbc.passerConsommationWeb(agent.getLogin(), "SERVICE", ControlleurApplication.APPLICATION_CYSEND,
                                                tSave.getMontant(), dev.getCodeiso(), tSave.getId());
                                    }catch(Exception ex){
                                        System.out.println("---------------------------------- CYSEND : "+ex.getMessage());
                                        System.out.println("-------------------------ECHEC DE LA CONSOMMATION---------------");
                                        ex.printStackTrace();
                                        //throw new CustomException("500", "Le transfer a abouti, cependant une erreur s'est produite durant le calcul des commissions");
                                    }
                                    
                                    /*****************BONUS***************/
                                             
                                    ControlleurBonusAgent ctrlBonus = new ControlleurBonusAgent();
                                    ControleurDevise ctrlD = new ControleurDevise();
                                    ControleurProduit controleurProduit = new ControleurProduit();
                                    Produit prod = controleurProduit.getProduit(daomanagers.ControleurProduit.PRODUIT_CYSEND);
                                    
                                    ctrlBonus.sendBonusAgent(agent, daomanagers.ControleurProduit.PRODUIT_CYSEND, prod.getIdproduit(),
                                            tSave.getMontant(), dev, tSave.getId());
                                    
                                    
                                    
                                }
                                else if(cysendStatus.equalsIgnoreCase("accepted"))
                                {
                                    tSave.setStatut("EN COURS");
                                    error.setCodeAndDescription("200", "EN COURS");
                                }
                                else
                                {
                                    tSave.setStatut("FAILED");
                                    error.setCodeAndDescription("400", tSave.getResponsedescription());
                                }
                                
                                
                                res.setResponse(response);
                            }
                            else
                            {
                                JSONObject js = resOrder.getResponse().getJSONObject("js");
                                tSave.setResponsecode(js.getString("code"));
                                tSave.setResponsedescription(js.getString("message"));
                                tSave.setResponsetitle(js.getString("information"));
                                
                                tSave.setStatut("FAILED");
                                
                                error.setCodeAndDescription("400", tSave.getResponsedescription());
                            }
                            
                           ctrlTrans.save(tSave);
                        }
                        else
                        {
                            error.setCodeAndDescription("400", "ERREUR LORS DE L'ENREGISTREMENT DE LA TRANSACTION");
                            System.out.println("ECHEC TRANSACTION : "+error);
                        }
                        
                        
                        
                    
                    } catch (Exception e) {
                        e.printStackTrace();
                        error.setCodeAndDescription("400", e.toString());
                    }
                    
                }
                else
                {
                    error.setCodeAndDescription("400", "INFO BENEFICIAIRE INCORRECT");
                    System.out.println("ECHEC TRANSACTION : "+error);
                }


            }
        }
        else
        {
            
            error = estimation.getError();
            System.out.println("ECHEC TRANSACTION : "+error);
        }
        
        res.setError(error);
        

        
        return res;
    }
    
}
