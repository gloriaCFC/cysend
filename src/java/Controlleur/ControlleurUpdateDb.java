/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlleur;

import Entities.CategorieEntity;
import Entities.FaceValueEntity;
import Entities.InfoBeneficiaireEntity;
import Entities.ListFaceValueEntity;
import Entities.ParametreEntity;
import Entities.PaysEntity;
import Entities.ProduitEntity;
import cfcdatabase.Model.Agent;
import cfcdatabase.Model.Categoriecy;
import cfcdatabase.Model.Facevaluecy;
import cfcdatabase.Model.Fraisproduitcy;
import cfcdatabase.Model.Infobeneficiaire;
import cfcdatabase.Model.Pays;
import cfcdatabase.Model.Payscategoriecy;
import cfcdatabase.Model.Produitcategoriecy;
import cfcdatabase.Model.Produitcy;
import cfcdatabase.Model.Produitpayscy;
import daomanagers.ControleurCategoriecy;
import daomanagers.ControleurFacevalue;
import daomanagers.ControleurFraisProduitcy;
import daomanagers.ControleurInfobeneficiaire;
import daomanagers.ControleurPaysCategorie;
import daomanagers.ControleurProduitCategoriecy;
import daomanagers.ControleurProduitcy;
import daomanagers.ControleurProduitpayscy;
import daomanagers.ControlleurPays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import utility.ErrorResponse;
import utility.HttpDataResponse;
import utility.Utils;

/**
 *
 * @author g.mbuyi
 */
public class ControlleurUpdateDb {

    public ControlleurUpdateDb() {
    }
    
    public int SavePays()
    {
        int nb = 0;
        
        ControlleurPays ctrlPays = new ControlleurPays();
        
        Map<String,Pays> listPays = ctrlPays.ToMapPays(ctrlPays.getListWithIso3Notnull());
        System.out.println("liste pays from DB : "+listPays.size());
        
        List<Pays> listPaysToSave = new ArrayList<>();
        try {
            ControlleurRequest ctrlRequest = new ControlleurRequest();
            Map<String,PaysEntity> mapPaysCy =  ctrlRequest.getPaysFromCysend();
            System.out.println("-------------- NB pays CYsend : "+mapPaysCy.size());
            for(String iso3 : mapPaysCy.keySet())
            {
                PaysEntity paysCy = mapPaysCy.get(iso3);
                System.out.println("iso3 : "+iso3);
                if(listPays.containsKey(iso3))
                {
                    Pays paysDb = listPays.get(iso3);
                    if(paysDb.getIdcysend()== null | paysDb.getPromotion()== null)
                    {
                        paysDb.setIdcysend(new Long(paysCy.getCountry_id()));
                        paysDb.setPromotion(String.valueOf(paysCy.isPromotion()));
                        listPaysToSave.add(paysDb);
                    }
                }
                else
                {
                    Pays p = new Pays();
                    p.setCodeiso3(iso3);
                    p.setIdcysend(new Long(paysCy.getCountry_id()));
                    p.setIsocountrycd(paysCy.getCountry_iso_alpha2());
                    p.setNom(paysCy.getCountry_name());
                    p.setPromotion(String.valueOf(paysCy.isPromotion()));
                    p.setAudit("insert from CYSEND DB");
                    
                    listPaysToSave.add(p);
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        nb = listPaysToSave.size();
        
        System.out.println("----------------NB pays à Save : "+nb);
        
        nb = ctrlPays.saveListPays(listPaysToSave);
        
        System.out.println("----------------NB pays Save : "+nb);
        
        return nb;
    }
    
    public int SaveCategorie()
    {
        int nb=0;
        
        ControleurCategoriecy ctrlCat = new ControleurCategoriecy();
        Map<Long, Categoriecy> mapCategorieFromDb = ctrlCat.getMap(ctrlCat.getList());
        System.out.println("liste Categorie from DB : "+mapCategorieFromDb.size());
        
        List<Categoriecy> listCategorieToSave = new ArrayList<>();
        try {
            ControlleurRequest ctrlRequest = new ControlleurRequest();
            Map<Long , CategorieEntity> mapCat = ctrlRequest.getCategorieFromCysend();
            
            for(Long l : mapCat.keySet())
            {
                CategorieEntity cat = mapCat.get(l);
                if(!mapCategorieFromDb.containsKey(cat.getCategory_id()))
                {
                    Categoriecy categorie = new Categoriecy();
                    categorie.setCategoryid(new Long(cat.getCategory_id()));
                    categorie.setCategoryname(cat.getCategory_name());
                    categorie.setDate(new Date());
                    categorie.setStatut("ON");
                    categorie.setPromotion(String.valueOf(cat.isPromotion()));
                    listCategorieToSave.add(categorie);
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        nb = listCategorieToSave.size();
        
        System.out.println("----------------NB Categorie à Save : "+nb);
        
        nb = ctrlCat.saveListCategorie(listCategorieToSave);
        
        System.out.println("----------------NB Categorie Save : "+nb);
        
        
        return nb;
    }
    
    public int SaveProduit()
    {
        int nb = 0;
        String prefixUrl = "https://www.cysend.com";
        ControleurProduitcy ctrlproduit = new ControleurProduitcy();
        Map<Long, Produitcy> mapProduit = ctrlproduit.toMapProduit(ctrlproduit.getList());
        List<Produitcy> listToSave = new ArrayList<>();
        Map<Long,ProduitEntity> mapProduitCy = new TreeMap<>();
        try {
            ControlleurRequest ctrlReq = new ControlleurRequest();
            mapProduitCy = ctrlReq.getProduitFromCysend();
            
            for(Long i : mapProduitCy.keySet() )
            {
                ProduitEntity pe = mapProduitCy.get(i);
                Long y = new Long(i);
                if(mapProduit.containsKey(y))
                {
                    Produitcy p = mapProduit.get(y);
                    
                    if(!(p.getMaintenance().equalsIgnoreCase(String.valueOf(pe.isMaintenance()))
                       & p.getPromotion().equalsIgnoreCase(String.valueOf(pe.isPromotion()))))
                    {
                        p.setMaintenance(String.valueOf(pe.isMaintenance()));
                        p.setPromotion(String.valueOf(pe.isPromotion()));
                        
                        listToSave.add(p);
                    }
                }
                else
                {
                    Produitcy p = new Produitcy();
                    p.setDate(new Date());
                    p.setLogo(prefixUrl+pe.getLogo_url());
                    p.setMaintenance(String.valueOf(pe.isMaintenance()));
                    p.setPromotion(String.valueOf(pe.isPromotion()));
                    p.setProductid(new Long(pe.getProduct_id()));
                    p.setProductname(pe.getProduct_name());
                    p.setStatut("ON");
                    p.setTypeproduit(pe.getType());
                    
                    listToSave.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        nb = listToSave.size();
        
        System.out.println("----------------NB Produit à Save : "+nb);
        
        nb = ctrlproduit.SaveListProduitCy(listToSave);
        
        System.out.println("----------------NB Produit Save : "+nb);
        
        if(mapProduit.size()<0)
            mapProduit = ctrlproduit.toMapProduit(ctrlproduit.getList());
        
        this.SaveInfoBeneficiaire(mapProduit, mapProduitCy);
        
        return nb;
    }
    
    public int SaveInfoBeneficiaire(Map<Long,Produitcy> listProduit , Map<Long,ProduitEntity> listProduitEntity)
    {
        int nb = 0;
        
        try {
            
            ControleurInfobeneficiaire ctrlInfo = new ControleurInfobeneficiaire();
            List<Infobeneficiaire> listToSave = new ArrayList<>();
            for(Long i : listProduitEntity.keySet())
            {
                ProduitEntity pe = listProduitEntity.get(i);
                Produitcy p = listProduit.get(new Long(i));
                List<InfoBeneficiaireEntity> listInfo = pe.getBeneficiary_information();
                if(listInfo.size()>0)
                {
                    for(InfoBeneficiaireEntity ib : listInfo)
                    {
                        Infobeneficiaire info = new Infobeneficiaire();
                        List<Infobeneficiaire> lis = ctrlInfo.getList(p.getId());
                        Map<String,Infobeneficiaire> lsInfo = ctrlInfo.TopMapList(lis);
                        if(!lsInfo.containsKey(ib.getName()))
                        {
                            info.setDate(new Date());
                            info.setDescription(ib.getDescription());
                            info.setNom(ib.getName());
                            info.setPattern(ib.getPattern());
                            info.setProduitcy(p);
                            info.setRequired(String.valueOf(ib.isRequired()));
                            info.setStatut("ON");
                            info.setType(ib.getType());

                            listToSave.add(info);
                        }

                    }

                } 
            }

            System.out.println("----------------NB InfoBen à Save : "+listToSave.size());
            nb = ctrlInfo.SaveListInfoBen(listToSave);
            System.out.println("----------------NB InfoBen  Save : "+nb);

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return nb;
    }
    
    public int SaveFaceValue()
    {
        int nb = 0;
        
        ControleurFacevalue ctrl = new ControleurFacevalue();
        ControleurProduitcy ctrlProduit = new ControleurProduitcy();
        Map<Long, Facevaluecy> mapFvFromDb = ctrl.ToMapFacevalue(ctrl.getList());
        List<Facevaluecy> listToSave = new ArrayList<>();
        ListFaceValueEntity lsFacevalueEnt = new ListFaceValueEntity();
        Map<Long ,Produitcy > mapProduit = ctrlProduit.toMapProduit(ctrlProduit.getList());
        
        try {
            ControlleurRequest ctrlReq = new ControlleurRequest();
            lsFacevalueEnt = ctrlReq.getFacevalueFromCysend();
            
            Map<Long , FaceValueEntity> listRange  = lsFacevalueEnt.getListRange();
            Map<Long,FaceValueEntity> listFixed = lsFacevalueEnt.getListFixed();
            
            for(Long l : listFixed.keySet())
            {
                FaceValueEntity fve = listFixed.get(l);
                Facevaluecy fv = new Facevaluecy();
                Produitcy produit = mapProduit.get(new Long(fve.getProduct_id()));
                if(mapFvFromDb.containsKey(l))
                {
                    fv = mapFvFromDb.get(l);
                    
                }
                
                fv.setFacevalueid(new Long(fve.getFace_value_id()));
                fv.setCost(fve.getCost());
                fv.setCostcurrency(fve.getCost_currency());
                fv.setDate(new Date());
                fv.setFacevalue(fve.getFace_value());
                fv.setFacevaluecurrency(fve.getFace_value_currency());
                fv.setPromotion(String.valueOf(fve.isPromotion()));
                fv.setProduitcy(produit);
                fv.setStatut("ON");
                fv.setTypefacevalue("FIXED");

                listToSave.add(fv);
                
            }
            
            
            for(Long l : listRange.keySet())
            {
                FaceValueEntity fve = listRange.get(l);
                Facevaluecy fv = new Facevaluecy();
                Produitcy produit = mapProduit.get(new Long(fve.getProduct_id()));
                if(mapFvFromDb.containsKey(l))
                {
                    fv = mapFvFromDb.get(l);
                    
                }
                
                fv.setProduitcy(produit);
                fv.setFacevalueid(new Long(fve.getFace_value_id()));
                fv.setFacevaluefrom(new Double(fve.getFace_value_from()));
                fv.setFacevalueto(new Double(fve.getFace_value_to()));
                fv.setFacevaluestep(fve.getFace_value_step());
                fv.setFacevaluecurrency(fve.getFace_value_currency());
                fv.setMaximumcost(fve.getMaximum_cost());
                fv.setMinimumcost(fve.getMinimum_cost());
                fv.setCostcurrency(fve.getCost_currency());
                fv.setPromotion(String.valueOf(fve.isPromotion()));
                fv.setDate(new Date());
                fv.setStatut("ON");
                fv.setTypefacevalue("RANGE");
                
                listToSave.add(fv);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        nb = listToSave.size();
        
        System.out.println("----------------NB Facevalue à Save : "+nb);
        
        nb = ctrl.SaveListFaceValue(listToSave);
        
        System.out.println("----------------NB Facevalue Save : "+nb);
        
        this.toOFFFacevalue(mapFvFromDb, lsFacevalueEnt);
        
        
        return nb;
    }
    
    public int toOFFFacevalue(Map<Long, Facevaluecy> mapFvFromDb , ListFaceValueEntity lsFacevalueEnt)
    {
        int nb = 0;
        ControleurFacevalue ctrl = new ControleurFacevalue();
        List<Facevaluecy> listToSave = new ArrayList<>();
        
        Map<Long , FaceValueEntity> listRange = lsFacevalueEnt.getListRange();
        Map<Long,FaceValueEntity> listFixed = lsFacevalueEnt.getListFixed();
        
        for(long id : mapFvFromDb.keySet())
        {
            Facevaluecy f = mapFvFromDb.get(id);
            if(f.getTypefacevalue().equals("FIXED")&!listFixed.containsKey(id))
            {
                f.setStatut("OFF");
                listToSave.add(f);
            }
            else if(f.getTypefacevalue().equals("RANGE")&!listRange.containsKey(id))
            {
                f.setStatut("OFF");
                listToSave.add(f);
            }
        }
        System.out.println("NB Facevalue à mettre à OFF : "+listToSave.size());
        
        nb = ctrl.SaveListFaceValue(listToSave);
        return nb;
        
    }
    
    public int SavePaysCategorie()
    {
        int nb = 0;
        
        ControleurPaysCategorie ctrl = new ControleurPaysCategorie();
        ControleurCategoriecy ctrlCat = new ControleurCategoriecy();
        ControlleurPays ctrlPays = new ControlleurPays();
        Map<String,Payscategoriecy> map = ctrl.TomapPaysCategorie(ctrl.getList());
        List<Payscategoriecy> listToSave = new ArrayList<>();
        
        try {
            ControlleurRequest ctrlRequest = new ControlleurRequest();
            Map<String,PaysEntity> mapPaysCy =  ctrlRequest.getPaysFromCysend();
            Map<Long, Categoriecy> mapCategorieFromDb = ctrlCat.getMap(ctrlCat.getList());
            Map<Long,Pays> mapPaysFromDb = ctrlPays.getPaysIdCysend();
            
            for(String key : mapPaysCy.keySet())
            {
                PaysEntity pe = mapPaysCy.get(key);
                int idPays = pe.getCountry_id();
                List<Integer> listCat = pe.getCategory_ids();
                
                for(Integer idCat : listCat)
                {
                    String keyPaysCat = idPays+"_"+idCat;
                    System.out.println("idpays_idCat : "+keyPaysCat);
                    
                    if(!map.containsKey(keyPaysCat))
                    {
                        Pays p = mapPaysFromDb.get(new Long(idPays));
                        Categoriecy cat = mapCategorieFromDb.get(new Long(idCat));
                        if(p!=null & cat!=null)
                        {
                            Payscategoriecy pc = new Payscategoriecy();
                            pc.setCategoriecy(cat);
                            pc.setDate(new Date());
                            pc.setPays(p);
                            pc.setStatut("ON");
                            listToSave.add(pc);
                        }
                        else
                        {
                            System.out.println("paysCategory not add to Listsave");
                            System.out.println("pays : "+p);
                            System.out.println("category : "+cat);
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        nb = listToSave.size();
        
        System.out.println("----------------NB paysCategorie à Save : "+nb);
        
        nb = ctrl.SaveList(listToSave);
        
        System.out.println("----------------NB payscategorie Save : "+nb);
        
        return nb;
    }
    
    public int SaveProduitCategorie()
    {
        int nb=0;
        
        ControleurProduitCategoriecy ctrl = new ControleurProduitCategoriecy();
        ControleurProduitcy ctrlProduit = new ControleurProduitcy();
        ControleurCategoriecy ctrlCat = new ControleurCategoriecy();
        
        List<Produitcategoriecy> listToSave = new ArrayList<>();
        
        try {
            ControlleurRequest ctrlReq = new ControlleurRequest();
            Map<String,Produitcategoriecy> mapProduitCat = ctrl.ToMapList(ctrl.getList());
            Map<Long,Produitcy> mapProduit = ctrlProduit.toMapProduit(ctrlProduit.getList());
            Map<Long,Categoriecy> mapCategorie = ctrlCat.getMap(ctrlCat.getList());
            
            Map<Long , CategorieEntity> mapCatFromCy = ctrlReq.getCategorieFromCysend();
            
            for(Long idC : mapCatFromCy.keySet())
            {
                CategorieEntity catE = mapCatFromCy.get(idC);
                List<Long> listProd = catE.getProduct_ids();
                
                for(Long idP : listProd)
                {
                    String key = idP+"_"+idC;
                    if(!mapProduitCat.containsKey(key))
                    {
                        Produitcy produit = mapProduit.get(new Long(idP));
                        Categoriecy categorie = mapCategorie.get(new Long(idC));
                        
                        Produitcategoriecy pc = new  Produitcategoriecy();
                        pc.setCategoriecy(categorie);
                        pc.setDate(new Date());
                        pc.setProduitcy(produit);
                        pc.setStatut("ON");
                        
                        listToSave.add(pc);
                    }
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
         nb = listToSave.size();
        
        System.out.println("----------------NB produitCategorie à Save : "+nb);
        
        nb = ctrl.SaveList(listToSave);
        
        System.out.println("----------------NB produitcategorie Save : "+nb);
        
        return nb;
        
    }
    
    public int SaveProduitPays()
    {
        int nb=0;
        
        ControleurProduitpayscy ctrl = new ControleurProduitpayscy();
        ControleurProduitcy ctrlProduit = new ControleurProduitcy();
        ControlleurPays ctrlPays = new ControlleurPays();
        
        List<Produitpayscy> listToSave = new ArrayList<>();
        
        try
        {
            ControlleurRequest ctrlReq = new ControlleurRequest();
            Map<String,Produitpayscy> mapProduitPays = ctrl.ToMapList(ctrl.getList());
            Map<Long,Produitcy> mapProduit = ctrlProduit.toMapProduit(ctrlProduit.getList());
            Map<Long,Pays> mapPays = ctrlPays.getPaysIdCysend();
            
            
            Map<Long , ProduitEntity> mapProduitFromCy = ctrlReq.getProduitFromCysend();
            
            for(Long i : mapProduitFromCy.keySet())
            {
                ProduitEntity p = mapProduitFromCy.get(i);
                List<Long> listPays = p.getCountries();
                Long idProduit = new Long(p.getProduct_id());
                for(Long ii : listPays)
                {
                    Long idPays = new Long(ii);
                    String key = idProduit+"_"+idPays;
                    
                    if(!mapProduitPays.containsKey(key))
                    {
                        Produitcy produit = mapProduit.get(idProduit);
                        Pays pays = mapPays.get(idPays);
                        if(produit!=null & pays!=null)
                        {
                            Produitpayscy ppc = new Produitpayscy();

                            ppc.setDate(new Date());
                            ppc.setPays(pays);
                            ppc.setProduitcy(produit);
                            ppc.setStatut("ON");

                            listToSave.add(ppc);
                        }
                        else
                        {
                            System.out.println("idPays : "+idPays);
                            System.out.println("idProduit : "+idProduit);
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        nb = listToSave.size();
        
        System.out.println("----------------NB produitPays à Save : "+nb);
        
        nb = ctrl.SaveList(listToSave);
        
        System.out.println("----------------NB produitPays Save : "+nb);
        
        return nb;
    }
    
    public HttpDataResponse<List<ParametreEntity>> saveParamettre(Agent agent , Long idProduit , Double frais)
    {
        HttpDataResponse<List<ParametreEntity>> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500","KO");
        
        List<ParametreEntity> listParam = new ArrayList<>();
        
        try {
            ControleurFraisProduitcy ctrlFrais = new ControleurFraisProduitcy();
            ControleurProduitcy ctrlProduit = new ControleurProduitcy();
            
            Produitcy p = null;
            
            if(idProduit!=null)
            {
                p = ctrlProduit.getProduit(idProduit);
            }
            
            try {
                if(frais!=null)
                {
                    if(p==null)
                        ctrlFrais.saveFraisParDefaut(frais, agent);
                    else
                        ctrlFrais.saveFrais(p, frais, agent);

                    error.setCodeAndDescription("200", "OK");
                    
                    List<Fraisproduitcy> ls = ctrlFrais.getListFrais();
                    
                    for(Fraisproduitcy fr : ls)
                    {
                        ParametreEntity par = new ParametreEntity();
                        String nomAgent = fr.getAgent().getContact().getPrenom()+" "+fr.getAgent().getContact().getNom();
                        String produit = "Default";
                        if(fr.getProduitcy()!=null)
                            produit = fr.getProduitcy().getProductname();
                        
                        par.setAgent(nomAgent);
                        par.setDate(Utils.convertDateToString(fr.getDate()));
                        par.setFrais(fr.getFrais());
                        par.setProduit(produit);
                        
                        listParam.add(par);
                    }
                    
                }
                else
                {
                    error.setCodeAndDescription("400", "VEUILLEZ ENTRER UN MONTANT CORRECTE, FRAIS NULL");
                }
                
            } catch (Exception e) {
                error.setCodeAndDescription("500", e.toString());
                e.printStackTrace();
            }
            
        } catch (Exception e) {
            error.setCodeAndDescription("500", e.toString());
            
        }
        
        res.setError(error);
        res.setResponse(listParam);
        return res;
    }
    
    
    public HttpDataResponse<List<ParametreEntity>> getListParametre()
    {
        ControleurFraisProduitcy ctrlFrais = new ControleurFraisProduitcy();
        HttpDataResponse<List<ParametreEntity>> res = new HttpDataResponse<>();
        ErrorResponse error = new ErrorResponse("500","KO");
        
        List<ParametreEntity> listParam = new ArrayList<>();
        
        try {
            List<Fraisproduitcy> ls = ctrlFrais.getListFrais();
                    
            for(Fraisproduitcy fr : ls)
            {
                ParametreEntity par = new ParametreEntity();
                String nomAgent = fr.getAgent().getContact().getPrenom()+" "+fr.getAgent().getContact().getNom();
                String produit = "Default";
                if(fr.getProduitcy()!=null)
                    produit = fr.getProduitcy().getProductname();
                par.setAgent(nomAgent);
                par.setDate(Utils.convertDateToString(fr.getDate()));
                par.setFrais(fr.getFrais());
                par.setProduit(produit);

                listParam.add(par);
            }
            error.setCodeAndDescription("200", "OK");
        } catch (Exception e) {
            error.setCodeAndDescription("500", e.toString());
            e.printStackTrace();
        }
        
        res.setError(error);
        res.setResponse(listParam);
        
        return res;
    }
    
}
